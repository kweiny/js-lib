package com.js.lib.mybitsplus.configtemplate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


/**
 * Swagger3 配置文件  模版
 * @author Administrator
 *
 */
/*@Configuration*/
public class Swagger3Config {

    @Value("${swagger-ui.enabled}")
    private boolean enable;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("1. 全部")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createDeviceApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("2. 设备管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection.device"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }

    @Bean
    public Docket createCollectionApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("3. 数据采集")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection.collection"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createAdminApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("4. 权限管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection.admin"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createMonitorApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("5. 视频监控管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection.gb28181"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createProjectApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("6. 项目管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.js.detection.project"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
/*
    @Bean
    public Docket createRestStreamProxyApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("4. 拉流转发")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.genersoft.iot.vmp.vmanager.streamProxy"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createRestStreamPushApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("5. 推流管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.genersoft.iot.vmp.vmanager.streamPush"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }


    @Bean
    public Docket createServerApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("6. 服务管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.genersoft.iot.vmp.vmanager.server"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }
    @Bean
    public Docket createUserApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("7. 用户管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.genersoft.iot.vmp.vmanager.user"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .enable(enable);
    }*/

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("detection  接口文档")
                .description("监测管理系统")
                //.contact(new Contact("109461831", "648540858", "109461831@qq.com"))
                .version("1.1.3")
                .build();
    }
}
