package com.js.lib.mybitsplus;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 继承自LambdaQueryWrapper
 * @param <P>
 */
public class JsLbWrapper<P> extends LambdaQueryWrapper<P> {

    public JsLbWrapper(P entity) {
        super(entity);
    }
}
