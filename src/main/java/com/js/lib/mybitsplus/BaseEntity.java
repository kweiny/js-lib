package com.js.lib.mybitsplus;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.js.lib.validator.group.IDelete;
import com.js.lib.validator.group.IUpdate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 说明： 实体类基类
 * @类名: BaseEntity
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2021年10月28日上午10:32:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BaseEntity implements Serializable{
	/** */
	private static final long serialVersionUID = 7362928438770037381L;

	//主键，自增长新增不提供")
    @TableId(value = "id", type = IdType.AUTO)
	@NotNull(message = "ID不允许为空",groups={IDelete.class, IUpdate.class})
    private Long id;

	//备注")
	private String note;

	//创建时间",hidden=true)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;

	//更新时间",hidden=true)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;

}
