package com.js.lib.mybitsplus;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 扩展的mapper基类
 * 可写自定义SQL，并配合QueryWrapper使用
 * @param <T> 实体类
 * @param <I> 结果类
 * @param <P> 查询条件参数类
 */
public interface JsMapper<T,I,P> extends BaseMapper<T> {
    I selectInfoOne(@Param(Constants.WRAPPER) Wrapper queryWrapper);

    List<I> selectInfoList(@Param(Constants.WRAPPER) Wrapper queryWrapper);

    Page<I>  selectInfoPage(Page<P> page, @Param(Constants.WRAPPER) Wrapper queryWrapper);
}
