package com.js.lib.mybitsplus;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 扩展 IService P查询条件参数类，I 结果类，T实体类
 * @param <P>  查询条件参数类
 * @param <I>  结果类
 * @param <T>  实体类
 */
public interface IJsService<T,I,P> extends IService<T> {

    I getOneInfo(Wrapper<P> queryWrapper, boolean throwEx);

    List<I> listInfo(Wrapper<P> queryWrapper);

    List<I> listInfo();

    Page<I>  pageInfo(Page<P> page, Wrapper<P> queryWrapper);

    Page<I>  pageInfo(Page<P> page);

}
