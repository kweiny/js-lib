package com.js.lib.mybitsplus.page;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Data
@Accessors(chain = true)
public class QueryPage {
    //每页条数")
    private long size = 15;
    //当前页")
    private long current = 1;
    //升序排字段")
    //private List<String> ascs = new ArrayList<>();
    private String[] ascs;
    //降序排字段")
    private String[] desc;
}
