package com.js.lib.mybitsplus.page;


import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Collections;
import java.util.List;

/**
 * 结果集页
 * @param <I>
 */
@Data
@Accessors(chain = true)
public class ResultPage<I> {
    //每页条数")
    private long pageSize = 15;
    //当前页")
    private long currentPage = 1;
    //总条数")
    private long total = 0;
    //数据集")
    protected List<I> rows = Collections.emptyList();
}
