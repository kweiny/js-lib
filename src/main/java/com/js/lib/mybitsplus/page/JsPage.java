package com.js.lib.mybitsplus.page;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 分页类
 * @param <P> 查询条件类
 */
@Data
@Accessors(chain = true)
public class JsPage<P>{
    //页对象")
    private QueryPage page;
    //参数对象")
    private P where;
}
