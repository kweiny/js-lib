package com.js.lib.mybitsplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

public class JsWrapper<P> extends QueryWrapper<P> {
    public JsWrapper(P p) {
        super(p);
    }
}
