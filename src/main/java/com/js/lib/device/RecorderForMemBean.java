package com.js.lib.device;

import java.io.Serializable;
import lombok.Data;
/**
 * @类名: RecorderForMemBean
 * @说明: 执法仪封装类，存储于内存或redis
 *
 * @author   kenny
 * @Date	 2018年11月20日下午4:03:55
 * 修改记录：
 *
 * @see 	 
 */
@Data
public class RecorderForMemBean implements Serializable {
	/**	 */
	private static final long serialVersionUID = 7562454209002633540L;
	
	private Long id;
	private String code;
	private String Name;
	private String policeCode;
	private String policeName;
	private String orgCode;
	private String orgName;
	private String path;
	private String status; 
	private Integer state=1;// 1、正常，2、报修，3、遗失，4、报废   5 未配备/未启用
	private String rtspUrl;
	private String channelNo;
	private String alarmChannelNo;//报警通道号
	private String gsfKey;//4G平台的key
	private String nationalStandard;//国标号
	private String lastOnlineTime;//最后在线时间, 系统刚启动里取发GPS的最后时间，以后就是实时时间

}
