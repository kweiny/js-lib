package com.js.lib.device;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @类名: ResultGPSInfo
 * @说明: GPS信息结果类
 *
 * @author   kenny
 * @Date	 2018年11月13日下午2:19:32
 * 修改记录：
 *
 * @see 	 
 */
@Data
@Accessors(chain=true)
public class ResultGPSInfo implements Serializable{
	private static final long serialVersionUID = 8883495695689143440L;
	
	private String DeviceID;
	private GPSInfo GpsInfo;
	/** 服务 器当前时间，不是设备传上来的时间 **/
	private Long currTime;
	
	public ResultGPSInfo(){
		super();
	}

	public ResultGPSInfo(String deviceID,String time,String longitude,String latitude){
		super();
		GPSInfo g = new GPSInfo();
		g.setTime(time);
		g.setLongitude(longitude);
		g.setLatitude(latitude);
		this.setGpsInfo(g);
		this.currTime = new Date().getTime();
		this.DeviceID = deviceID;
	}


	@Data
	public static class GPSInfo {
		private String Time;//时间
		private String Longitude;//经度"123"
		private String Latitude;//纬度"123"
	}
	
}
