package com.js.lib.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.js.lib.cache.listener.AbstractMessageListener;
import com.js.lib.cache.listener.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.js.lib.cache.Cache;
import com.js.lib.config.RedisConfigProperties;
import lombok.extern.slf4j.Slf4j;
/**
 * 说明：
 * @类名: SysCache
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2022年1月11日下午1:43:37
 */
@Slf4j
@Service("sysCache")
public class SysCache implements Cache{
	private static SysCache instance;
	/** 本地缓存或 redis缓存 */
	private Cache cache=null;

	@Resource(name="localCache")
	private Cache localCache;

	@Resource(name="redisCache")
	private Cache redisCache;

	@Autowired
	private RedisConfigProperties redisPropertis;

/*	public SysCache(){
		instance = this;
		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
			try{
				this.clearExpireData();
			}catch(Exception e){
				log.error(">>>>>>>>>>>> SysCache.clearExpireData:{}" + e.toString());
			}
		}, 10, 60, TimeUnit.SECONDS);
	}*/

	@PostConstruct
	public void SysCache(){
		if (this.isRedis() && redisCache!=null){
			this.cache = redisCache;
		}
		if (this.cache == null){
			this.cache = localCache;
		}
		instance = this;
		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
			try{
				this.clearExpireData();
			}catch(Exception e){
				log.error(">>>>>>>>>>>> SysCache.clearExpireData:{}" + e.toString());
			}
		}, 10, 60, TimeUnit.SECONDS);
	}

	public static synchronized SysCache getInstance() {
		if (instance == null) {
			instance = new SysCache();
		}
		return instance;
	}

	/** 是否配置了redis*/
	private boolean isRedis(){
		return redisPropertis != null && redisPropertis.isActive();
	}

	@Override
	public Map<Object,Object> viewData(){
		return cache.viewData();
	}

	@Override
	public String getCacheType() {
		return cache.getCacheType();
	}

	@Override
	public Object get(String key) {
		if (StringUtils.isEmpty(key)){
			log.error(">>>>>> SysCache: key 必须有值！");
			return null;
		}
		return cache.get(key);
	}
	@Override
	public void set(String key, Object object) {
		if (StringUtils.isEmpty(key)){
			log.error(">>>>>> SysCache: key 必须有值！");
			return;
		}

		cache.set(key, object);

	}

	@Override
	public void addListener(AbstractMessageListener messageListener) {
		cache.addListener(messageListener);
	}

	@Override
	public void removeListener(AbstractMessageListener messageListener) {
		cache.removeListener(messageListener);
	}

	@Override
	public void clearListener() {
		cache.clearListener();
	}

	@Override
	public void set(String key, Object object, Long time) {
		if (StringUtils.isEmpty(key)){
			log.error(">>>>>> SysCache: key 必须有值！");
			return;
		}
		cache.set(key, object,time);

	}

	@Override
	public void remove(String key) {
		if (StringUtils.isEmpty(key)){
			log.error(">>>>>> SysCache: key 必须有值！");
			return;
		}
		cache.remove(key);

	}

	@Override
	public Boolean del(String key) {
		remove(key);
		return true;
	}

	@Override
	public void hset(String key, String item, Object value, Long time) {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(item)){
			log.error(">>>>>> SysCache: key 和  item 必须有值！");
			return ;
		}
		cache.hset(key, item, value,time);
	}
	@Override
	public void hset(String key, String item, Object value) {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(item)){
			log.error(">>>>>> SysCache: key 和  item 必须有值！");
			return ;
		}

		cache.hset(key, item, value);

	}
	@Override
	public Object hget(String key, String item) {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(item)){
			log.error(">>>>>> SysSource: key 和  item 必须有值！");
			return null;
		}

		return cache.hget(key, item);
	}
	@Override
	public Map<Object, Object> hmget(String key) {
		if (StringUtils.isEmpty(key)){
			log.error(">>>>>> SysCache: key 必须有值！");
			return null;
		}
		return cache.hmget(key);
	}
	//@Override
	public void hdel(String key, Object... items) {
		if (StringUtils.isEmpty(key) || items==null || items.length == 0){
			log.error(">>>>>> SysCache: key 和  item 必须有值！");
			return ;
		}
		cache.hdel(key, items);

	}
	//@Override
	public Long setExpire(String key, String item, Long time) {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(item)){
			log.error(">>>>>> SysCache: key 和  item 必须有值！");
			return null;
		}
		return cache.setExpire(key, item, time);

	}
	//@Override
	public void clearExpireData() {
		cache.clearExpireData();

	}
	//@Override
	public Object showMaps(String key) {
		return cache.showMaps(key);
	}
	//@Override
	public Object showExpireData() {
		return cache.showExpireData();
	}
	//@Override
	/**
	 * 自动清除过期键
	 * @param interval 间隔时间 单位秒
	 */
	public void autoClearExpire(Long interval) {
		log.info(String.format(">>>>>>>>>>>>>>>>>>启动定时清理过期键, 定时时长 %s ................",interval));
		cache.autoClearExpire(interval);

	}
	/**
	 * 模糊查询
	 * @param key
	 * @return
	 */
	@Override
	public List<Object> scan(String key) {
		return cache.scan(key);
	}

	@Override
	public Boolean hasKey(String key) {
		return cache.hasKey(key);
	}

	@Override
	public void zAdd(Object key, Object value, double score) {cache.zAdd(key, value, score);}

	@Override
	public void zRemove(Object key, Object value) {cache.zRemove(key, value);	}

	@Override
	public Double zIncrScore(Object key, Object value, double delta) {		return cache.zIncrScore(key, value, delta);	}

	@Override
	public Double zScore(Object key, Object value) {return cache.zScore(key, value);}

	@Override
	public Long zRank(Object key, Object value) {return cache.zRank(key, value);}

	@Override
	public Long zSize(Object key) {	return cache.zSize(key);	}

	@Override
	public Set<Object> zRange(Object key, int start, int end) {	return cache.zRange(key, start, end);}

	@Override
	public Set<Object> zRevRange(Object key, int start, int end) {
		return cache.zRevRange(key, start, end);
	}

	@Override
	public Set<Object> zSortRange(Object key, int min, int max) {
		return cache.zSortRange(key, min, max);
	}
}
