package com.js.lib.cache;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * 有序集合类
 */
@Slf4j
public class ZKeyPacket {
    /** 真实数据 */
    private Object key;
    private ConcurrentHashMap<Object,Object> valueScoreMap = new ConcurrentHashMap<Object,Object>();
    private ConcurrentSkipListMap<Object, List<Object>> zsetMap = new ConcurrentSkipListMap<Object,List<Object>>();

    public ZKeyPacket(){}
    public ZKeyPacket(Object key){this.key=key;};

    public boolean isEmpty(){
        return valueScoreMap.size() <= 0;
    }

    /**
     * 增加新的
     * @param value
     * @param score
     */
    public void add(Object value,double score){
        /** 更新 score 值 */
        valueScoreMap.put(value.hashCode(),score);
        List<Object> valueList = null;
        Object obj=zsetMap.get(score);
        if (obj == null){
            valueList = new ArrayList<>();
        }else{
            valueList = (List<Object>)obj;
        }
        valueList.add(value);
        zsetMap.put(score,valueList);
        log.debug(String.format("add: %s,%s",value,score));
    }

    /**
     * 删除
     * @param value
     * @return  value 的长度，如果是0 表示没有保存数据
     */
    public int remove(Object value){
        int result = valueScoreMap.size();
        /** 取得score */
        Object score=valueScoreMap.get(value.hashCode());
        if (score == null){return result;}
        valueScoreMap.remove(value.hashCode());

        /** 取得之前保存的 value */
        this.removeValueFromList(score,value);
        if (valueScoreMap.size() == 0){
            //valueScoreMap = null;
            return -1;
        }
        log.debug(String.format("remove: %s",value));
        return valueScoreMap.size();
    }

    /**
     * 修改 score + delta
     * @param value
     * @param delta
     * @return
     */
    public Double IncrScore(Object value, double delta){
        /** 取得旧score */
        Object originScore=valueScoreMap.get(value.hashCode());
        if (originScore == null){return null;}

        /** 不要小于0 */
        double newScore = (double)originScore;// Double.valueOf(originScore.toString());
        newScore = newScore + delta;
        if ((newScore) < 0){
            newScore = 0;
        }

        /** 如果 值没有改变就中断退出 */
        if (newScore == (double)originScore){
            return newScore;
        }
        /** 先加入新值 */
        this.add(value,newScore);

        /** 再移旧值  */
        this.removeValueFromList(originScore,value);

        log.debug(String.format("IncrScore: %s,%s",value,delta));
        return  newScore;
    }

    /**
     * 查询value对应的score   zscore
     * @param value
     * @return
     */
    public Double zScore(Object value) {
        Object obj = valueScoreMap.get(value.hashCode());
        if (obj == null){return null;}
        return Double.valueOf(obj.toString());
    }

    /**
     * 判断value在zset中的排名  zrank
     * @param value
     * @return
     */
    public Long zRank(Object value) {
        Object obj = valueScoreMap.get(value.hashCode());
        if (obj == null){return null;}
        double to = Double.valueOf(obj.toString());
        Long order=null;
        ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(0.0,true,to,true);
        for (Map.Entry<Object,List<Object>> entry : subMap.entrySet()) { //取一定范围的集合
            if (Double.valueOf(entry.getKey().toString())< to){
                order = order + entry.getValue().size();
            }else{
                for(Object val : entry.getValue()){
                    order++;
                    if (val.hashCode() >= val.hashCode()){
                        return order;
                    }
                }
            }

        }
        return order;
    }

    /**
     * 返回集合的长度
     * @return
     */
    public Long zSize() {
        return Long.valueOf(valueScoreMap.size());
    }
    /**
     * 查询集合中指定顺序的值， 0 -1 表示获取全部的集合内容  zrange
     * 返回有序的集合，score小的在前面
     * @param start
     * @param end
     * @return
     */
    public Set<Object> ZRange(int start, int end) {
        double dStart = Double.valueOf(start);
        double dEnd = Double.valueOf(end);
        /** LinkedHashSet 可以按写入顺序 来输出 */
        Set<Object> result = new LinkedHashSet<Object>();

        if (start == 0 && end == -1){
            /** 全部 */
            for (Map.Entry<Object,List<Object>> entry : zsetMap.entrySet()) { //取一定范围的集合
                result.addAll(entry.getValue());
            }
        }else{
            /** >= 和 <= */
            ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(dStart,true,dEnd,true);
            for (Map.Entry<Object,List<Object>> entry : subMap.entrySet()) { //取一定范围的集合
                result.addAll(entry.getValue());
            }
        }

        return result.size()>0?result:null;
    }

    /**
     * 查询集合中指定顺序的值  zrevrange
     * 倒序排,返回有序的集合中，score大的在前面
     * @param start
     * @param end
     * @return
     */
    public Set<Object> zRevRange(int start, int end) {
        double dStart = Double.valueOf(start);
        double dEnd = Double.valueOf(end);

        Set<Object> result = new LinkedHashSet<>();
        if (start == 0 && end == -1){
            /** 全部 */
            ConcurrentNavigableMap<Object, List<Object>> subMap1 = zsetMap.descendingMap();
            for (Map.Entry<Object,List<Object>> entry : subMap1.entrySet()) { //取一定范围的集合
                result.addAll(entry.getValue());
            }
        }else {

            /** 选择出范围 */
            ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(dStart,true, dEnd,true);
            /** 倒序排 */
            ConcurrentNavigableMap<Object, List<Object>> subMap1 = subMap.descendingMap();
            for (Map.Entry<Object, List<Object>> entry : subMap1.entrySet()) { //取一定范围的集合
                result.addAll(entry.getValue());
            }
        }


        //System.err.println(result.toString());
        return result.size()>0?result:null;
    }

    /**
     * List 中移除 Value
     * @param score
     * @param value
     */
    private void removeValueFromList(Object score,Object value){
        /** 取得之前保存的 value */
        Object obj = zsetMap.get(score);
        if (obj == null){return;}

        List<Object> valueList = (List<Object>)obj;
        valueList.remove(value);

        if (valueList.size()==0){
            zsetMap.remove(score);
        }
        /** 移除 score 后再移除 value */
        obj = valueScoreMap.get(value.hashCode());
        if (obj != null){
            if ((double)obj == (double)score){
                valueScoreMap.remove(value.hashCode());
            }
        }

    }

    /**
     * 加 Value 到新的List 中
     * @param score
     * @param value
     */
    private void addValueToList(Object score,Object value){
        Object obj = zsetMap.get(score);
        if (obj == null){return;}
        List<Object> valueList = (List<Object>)obj;
        valueList.add(value);
    }

    public Map<Object,Object> view(){
        Map<Object,Object> resultMap = new HashMap<Object,Object>();
        resultMap.put("key",this.key);
        Map<Object,Object> vsMap = new HashMap<>();
        vsMap.putAll(valueScoreMap);
        resultMap.put("values",vsMap);
        Map<Object,Object> zMap = new HashMap<>();
        zMap.putAll(zsetMap);
        resultMap.put("scores",zMap);
        return resultMap;
    }
}
