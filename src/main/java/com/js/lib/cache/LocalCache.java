package com.js.lib.cache;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import com.alibaba.fastjson2.JSONObject;
import com.js.lib.cache.listener.AbstractMessageListener;
import com.js.lib.cache.listener.EventType;
import com.js.lib.cache.listener.MessageListener;
import com.js.lib.utils.ObjectToByteUtils;
import com.js.lib.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 说明：
 * @类名: LocalCache
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2022年1月11日上午11:51:38
 */
@Slf4j
@Service("localCache")
public class LocalCache implements Cache{

	private static LocalCache instance;
	private ConcurrentHashMap<String,Object> mainMap = new ConcurrentHashMap<String,Object>();
	private ConcurrentSkipListMap<Object,Object> expireMap = new ConcurrentSkipListMap<Object,Object>();
	private ListenerHelper listenerHelper = new ListenerHelper();
	private int clearTimes=0; //过期清理次数， 每10次做一次深度清理
	private final int MAX_CLEAR_TIMES=20;


	private ClearRunnable clearRunnable = null;

	private long interval = 10000L;

	private ZLocalCache zLocalCache = ZLocalCache.getInstance();
	/**  */
	private volatile boolean isbusy=false;

	@PostConstruct
	public void LocalCache(){
		instance = this;
	}

	@Override
	public String getCacheType(){
		return "local";
	}
	public static synchronized LocalCache getInstance() {
		if (instance == null) {
			instance = new LocalCache();
		}
		return instance;
	}

	/** 加入监听器 */
	public void addListener(AbstractMessageListener messageListener){
		listenerHelper.addListener(messageListener);
	}
	/** 移除监听器 */
	public void removeListener(AbstractMessageListener messageListener){
		listenerHelper.removeListener(messageListener);
	}
	/** 清除监听器 */
	public void clearListener(){
		listenerHelper.clearListener();
	}

	@Override
	public Map<Object,Object> viewData(){
		Map<Object,Object> map = new HashMap<>();
		map.put("main",mainMap);
		map.put("zset",zLocalCache.viewData());
		return map;
	}


	/**
	 * 取得值
	 * @param key  key 值
	 * @return
	 */
	public Object get(String key) {
		return getValue(key,null);
	}
	/**
	 * 保存
	 * @param name
	 * @param object
	 */
	public void set(String name, Object object) {
		setValue(name,null,object,null);
	}
	public void set(String name, Object object,Long time) {
		setValue(name,null,object,time);
	}

	/*
	 * 删除 key, 可能有单值，可能有复合集
	 *
	 */
	public void remove(String key){
		Object obj=null;
		/** 先删除expireMap */
		if ((obj=mainMap.get(key))!=null){
			/** 单值  */
			if (obj instanceof DataPacket) {
				this.removeExpireMap(((DataPacket) obj).getExipreTime());
			}else if (obj instanceof Map){
				/** hash复合值 */
				Map<String,Object> tmpMap = (Map<String,Object>) obj;
				for (Map.Entry<String,Object> e : tmpMap.entrySet()){
					this.removeExpireMap(((DataPacket) e.getValue()).getExipreTime());
				}
			}
			mainMap.remove(key);

			/** 触发监听事件 */
			listenerHelper.removeEvent(key,null);
		}
	}

	@Override
	public Boolean del(String key) {
		this.remove(key);
		return true;
	}


	/**
	 * 设置map数据
	 * @param key   键
	 * @param item	项
	 * @param value	值
	 * @param time	失效时间（秒）
	 */
	public void hset(String key, String item,Object value, Long time) {
		setValue(key,item,value,time);
	}
	public void hset(String key, String item,Object value) {
		setValue(key,item,value,null);
	}

	/**
	 * 取hash数据
	 * @param key
	 * @param item
	 * @return
	 */
	public Object hget(String key, String item) {
		return getValue(key,item);
	}

	public Map<Object,Object> hmget(String key) {
		Map<Object,Object> map=null;
		Object obj = mainMap.get(key);
		if (obj != null){
			map = (Map<Object,Object>) obj;
			Map<Object,Object> result = new HashMap<Object,Object>();
			for (Map.Entry<Object,Object> e : map.entrySet()){
				result.put(e.getKey(),((DataPacket) e.getValue()).getData());
			}
			return result.size()>0?result:null;
		}
		return null;
	}

	/**
	 * 删除collect数据
	 * @param key
	 * @param items
	 */
	public void hdel(String key, Object... items) {
		if (StringUtils.isEmpty(key) || items==null || items.length==0){
			return;
		}
		Object obj = null;
		if ((obj=mainMap.get(key))!=null){
			Map<String,Object> tempMap = (Map<String,Object>) obj;
			for (Object o : items){
				/**先删除expireMap*/
				if (tempMap.get(o)!= null && tempMap.get(o) instanceof DataPacket)
					this.removeExpireMap(((DataPacket) tempMap.get(o)).getExipreTime());
				tempMap.remove(o);
				/** 触发监听事件 */
				listenerHelper.removeEvent(key,o);
			}
			if (tempMap.size() == 0){
				/** 如果子项已被清空 */
				mainMap.remove(key);
			}
		}
	}

	/**
	 * 清理过期
	 */
	@SuppressWarnings("unchecked")
	public void clearExpireData(){
		log.debug(">>>>>>>>>>清理过期键>>>>>>>>>>>>>>");
		if (isbusy){return;}
		try{
			isbusy = true;
			Long from = 0L,to=System.currentTimeMillis()*1000;//毫秒变微秒
			Object obj=null;
			JSONObject keyItem = null;
			Object key=null;Object item=null;
			ConcurrentNavigableMap<Object,Object> subMap = expireMap.subMap(from,to);
			for (Entry<Object,Object> entry : subMap.entrySet()) { //取一定范围的集合
				keyItem = JSONObject.parseObject(entry.getValue().toString(), JSONObject.class);
				key =  keyItem.get("key");
				item =  keyItem.get("item");

				if (item != null){
					if ((obj=mainMap.get(key))!=null){
						((Map<String,Object>)obj).remove(item);
					}
				}else{
					mainMap.remove(key);
				}
				expireMap.remove(entry.getKey());
				/** 触发 过期监听 */
				//log.debug("ExpireEvent; {},{}",key,item);
				listenerHelper.ExpireEvent(key,item,null);
			}

			/** 深度清除过期键 */
			clearTimes++;
			if (this.clearTimes > this.MAX_CLEAR_TIMES){
				clearTimes=0;
				this.deepClearExpire();
			}

		}catch(Exception e){
			log.error(">>>>>>>>>>>clearExpireData: " + e.toString());
		}finally{
			isbusy = false;
		}
	}

	/**
	 * 深度清理过期键
	 * 隔一段时间跑一次，把漏残的过期项删除
	 */
	public void deepClearExpire(){
		List<JSONObject> expireKeys = new ArrayList<>();
		DataPacket dataPacket=null;
		JSONObject keyItem = null;
		Long currTime = System.currentTimeMillis()*1000; //当前时间，微秒
		Map<String,Object> itemMap = null;
		Iterator entries = mainMap.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry entry = (Entry) entries.next();
			if (entry.getValue() ==null) {continue;}
			if (entry.getValue() instanceof DataPacket){
				dataPacket = (DataPacket)entry.getValue();
				if (null== dataPacket.getExipreTime() || 0l==dataPacket.getExipreTime()){continue;}
				if (dataPacket.getExipreTime() >= currTime){continue;}
				//已过期
				keyItem = (JSONObject)expireMap.get(dataPacket.getExipreTime());
				if (keyItem != null){  continue;}

				JSONObject jsonObject = new JSONObject();
				jsonObject.put("key", entry.getKey());
				expireKeys.add(jsonObject);

			}else if (entry.getValue() instanceof Map){
				itemMap = (Map<String,Object>)entry.getValue();
				Iterator itemEntries = itemMap.entrySet().iterator();
				while (itemEntries.hasNext()) {
					Map.Entry itemEntry = (Entry) itemEntries.next();
					if (itemEntry.getValue() ==null) {continue;}
					if (itemEntry.getValue() instanceof DataPacket){
						dataPacket = (DataPacket)itemEntry.getValue();
						if (null== dataPacket.getExipreTime() || 0L==dataPacket.getExipreTime()){continue;}
						if (dataPacket.getExipreTime() >= currTime){continue;}

						keyItem = (JSONObject)expireMap.get(dataPacket.getExipreTime());
						if (keyItem != null){continue;}

						JSONObject jsonObject = new JSONObject();
						jsonObject.put("key", entry.getKey());
						jsonObject.put("item", itemEntry.getKey());
						expireKeys.add(jsonObject);
					}
				}
			}

			/* 如果有则删除 */
			for (JSONObject jsonObject : expireKeys){
				String key=(String)jsonObject.get("key");
				if (StringUtils.isEmpty(key)){
					continue;
				}
				String item=(String)jsonObject.get("item");
				if (StringUtils.isEmpty(item)){
					this.remove(key);
				}else{
					this.hdel(key,item);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void setValue(String key,String item,Object value,Long time){

		time = time==null || time.equals(0L)?0L:time;//this.toMicroSec(time);
		Object obj=null;
		Long originTime=null;
		/** 设置过期时间   */
		//log.debug("setValue: key={};item={};value={};time={}",key,item,value,time);
		time = setExpire(key,item,time);
		if (StringUtils.isEmpty(item)){
			/** 单值  */
			mainMap.put(key, new DataPacket(value, time));
		}else{
			/** hash值集体值  */
			Map<String,Object> tmpMap = null;
			if ((obj=mainMap.get(key)) != null){
				tmpMap = (Map<String,Object>) obj;
			}else{
				tmpMap = new HashMap<String,Object>();
			}
			tmpMap.put(item, new DataPacket(value, time));
			mainMap.put(key, tmpMap);
		}

		/** 处理监听事件 */
		listenerHelper.setEvent(key,item,value);
	}

	@SuppressWarnings("unchecked")
	private Object getValue(String key,String item){
		Object result=null;
		if (item == null){//单值
			result = mainMap.get(key);
			if (result != null){
				DataPacket dp = (DataPacket)result;
				return dp.getData();
			}
		}else{//集合值
			Object obj = mainMap.get(key);
			if (obj != null){
				Map<String,Object> map = (Map<String,Object>)obj;
				result = map.get(item);
				if (result != null){
					DataPacket dp = (DataPacket)result;
					return dp.getData();
				}
			}
		}
		return null;
	}
	/**
	 * 修改超时时间
	 * @param key
	 * @param item
	 * @param time 秒
	 */
	public Long setExpire(String key,String item,Long time){
		time = time==null || time.equals(0L)?0L:this.toMicroSec(time);
		Object obj=null;
		DataPacket dp=null;

		/** 删除旧的 时间，生成新的  */
		if ((obj = mainMap.get(key))!=null){
			if (StringUtils.isEmpty(item)){
				/** 单值  */
				dp=(DataPacket)obj;
				if (dp.getExipreTime() > 0L)
					expireMap.remove(dp.getExipreTime());
				dp.setExipreTime(time);
			}else{
				/** HASH 集合值 */
				Map<String,Object> map = (Map<String,Object>)obj;
				if ((obj = map.get(item))!=null){
					dp=(DataPacket)obj;
					if (dp.getExipreTime() > 0L)
						expireMap.remove(dp.getExipreTime());
					dp.setExipreTime(this.toMicroSec(time));
				}
			}
		}

		if (time.equals(0L)){return 0L;}

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("key", key);
		if (!StringUtils.isEmpty(item)){
			jsonObject.put("item", item);
		}

		expireMap.put(time, jsonObject);
		//log.debug("setExpire2: expireMap={}",expireMap);
		return time;
	}


	/**
	 * 移除过期时间
	 * @param time
	 */
	private void removeExpireMap(Long time){
		if (time == null || time.equals(0L)){
			return;
		}
		expireMap.remove(time);
	}


	/**
	 * 取得微秒
	 * @param incrementSec 增量时间，秒
	 * @return
	 */
	private Long toMicroSec(Long incrementSec){
		Long cutime = (System.currentTimeMillis() + incrementSec * 1000) * 1000; // 微秒
		Long nanoTime = System.nanoTime(); // 纳秒
		return cutime + (nanoTime - nanoTime / 1000000 * 1000000) / 1000;
	}
	/**
	 * 由微秒变回毫秒
	 * @param microSec
	 * @return
	 */
	private Long toMillisSec(Long microSec){
		return microSec/1000;
	}
	/**
	 * 显示 数据
	 */
	public Object showMaps(String key){
		Map<Object,Object> result = new HashMap<>();
		if (key == null){
			result.put("main",mainMap);
			result.put("sortedSet",zLocalCache.getMainMap());
			//return mainMap;
		}
		else{
			result.put("main",
				mainMap.entrySet().parallelStream().filter(
					(e)->{
						if (e.getKey().contains(key)){
							return true;
						}
						return false;
					}).collect(Collectors.toMap(
							(e) -> (String) e.getKey(),
							(e) -> e.getValue()
					))
			);
			result.put("sortedSet",zLocalCache.getMainMap());
		}
		return result;
	}

	/**
	 * 显示超时 数据
	 */
	public Object showExpireData(){
		return expireMap;
	}


	/**
	 * 自动清除过期键
	 * @param interval 间隔时间 单位秒
	 */
	public void autoClearExpire(Long interval){
		if (interval != null || !interval.equals(0L))
			this.interval = interval*1000L;
		if (clearRunnable == null){
			clearRunnable = new ClearRunnable();
			new Thread(clearRunnable).start();
		}

	}
	/**
	 * 模糊查询
	 * 只针对 普通值，不针对map 或list
	 * @param key
	 * @return
	 */
	@Override
	public List<Object> scan(String key) {
		//Map<String,Object> map =
		List<Object> result = new ArrayList<>();
		mainMap.entrySet().stream().filter((e)->{
					/** 判断带 * 号的模糊查询 */
					String src = e.getKey();
					String[] subP = key.split("\\*");
					int begin =0;
					for(int i=0;i< subP.length;i++){
						begin = src.indexOf(subP[i]);// checkSubPattern(src,begin,subP[i]);
						if (begin == -1){return false;}
						if (subP.length == 1 && begin != 0){
							/** 不带星号，又不是从左开始排除，则不成立 */
							return false;
						}
						src = src.substring(begin+subP[i].length(),src.length());
					}
					return true;
				})
				.forEach(
				(e)->{
					//result.add(getValue(e.getKey(),null));
					result.add(e.getKey());
				}
		);
		return result;
	}

	@Override
	public Boolean hasKey(String key) {
		return mainMap.get(key)!=null;
	}

	/** ====================================有序集合================================================= */
	/**
	 * 添加一个元素, zset与set最大的区别就是每个元素都有一个score，因此有个排序的辅助功能;  zadd
	 * @param key
	 * @param value
	 * @param score
	 */
	public void zAdd(Object key, Object value, double score) {
		zLocalCache.zAdd(key,value,score);
	}
	/**
	 * 删除元素 zrem
	 *
	 * @param key
	 * @param value
	 */
	public void zRemove(Object key, Object value) {zLocalCache.zRemove(key,value);}
	/**
	 * score的增加or减少 zincrby
	 *
	 * @param key
	 * @param value
	 * @param delta -1 表示减 1 表示加1
	 */
	public Double zIncrScore(Object key, Object value, double delta) {return zLocalCache.zIncrScore(key, value, delta);}
	/**
	 * 查询value对应的score   zscore
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public Double zScore(Object key, Object value) {
		return zLocalCache.zScore(key, value);
	}
	/**
	 * 判断value在zset中的排名  zrank
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public Long zRank(Object key, Object value) {
		return zLocalCache.zRank(key, value);
	}
	/**
	 * 返回集合的长度
	 *
	 * @param key
	 * @return
	 */
	public Long zSize(Object key) {return zLocalCache.zSize(key);}

	/**
	 * 查询集合中指定顺序的值， 0 -1 表示获取全部的集合内容  zrange
	 *
	 * 返回有序的集合，score小的在前面
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Set<Object> zRange(Object key, int start, int end) {
		return zLocalCache.zRange(key, start, end);
	}
	/**
	 * 查询集合中指定顺序的值和score，0, -1 表示获取全部的集合内容
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
/*	public Set<ZSetOperations.TypedTuple<String>> zRangeWithScore(Object key, int start, int end) {
		return null;
	}*/
	/**
	 * 查询集合中指定顺序的值  zrevrange
	 *
	 * 返回有序的集合中，score大的在前面
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Set<Object> zRevRange(Object key, int start, int end) {
		return zLocalCache.zRevRange(key, start, end);
	}
	/**
	 * 根据score的值，来获取满足条件的集合  zrangebyscore
	 *
	 * @param key
	 * @param min
	 * @param max
	 * @return
	 */
	public Set<Object> zSortRange(Object key, int min, int max) {
		return zLocalCache.zSortRange(key, min, max);
	}

	/** ====================================内部接品和类================================================= */
	/**
	 * 清除内存中过期的数据
	 * @author 10946
	 *
	 */
	class ClearRunnable implements Runnable{
		public void run(){
			while (true){
				try{
					if (!isbusy) {clearExpireData();}
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}finally{
					isbusy = false;
				}
			}
		}
	}

	/** ================================== 数据包装 DataPacket =================================*/

	/**
	 * 存储的数据结构类
	 */
	public final class DataPacket{
		/** 真实数据 */
		private Object data;
		/** 过期时间 */
		private Long exipreTime;
		public DataPacket(Object data,Long exipreTime){
			this.data = data;
			this.exipreTime = exipreTime;
		}

		public String toString(){
			if (exipreTime > 0){
				return data.toString() + "#" + DateUtils.dateTimeMillecondFormat(new Date(toMillisSec(exipreTime)));
			}
			return data.toString();
		}

		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
		public Long getExipreTime() {
			return exipreTime;
		}
		public void setExipreTime(Long exipreTime) {
			this.exipreTime = exipreTime;
		}
	}

	/** ================================== 事件监听包装类 ListenerPacket =================================*/
	public final class ListenerHelper{
		private CopyOnWriteArrayList<ListenerPacket> listenerPackets = new CopyOnWriteArrayList<ListenerPacket>();

		/** 加入 监听器 */
		public void addListener(AbstractMessageListener messageListener){
			ListenerPacket listenerPacket = new ListenerPacket();
			listenerPacket.messageListener = messageListener;
			listenerPacket.asterisk=messageListener.getResolveKey().contains("*");

			/** *号表示监听所有键 */
			if (!"*".equals(messageListener.getResolveKey()))
				listenerPacket.subKeys = messageListener.getResolveKey().split("\\*");
			if (!"*".equals(messageListener.getResolveItem()))
				listenerPacket.subItems = messageListener.getResolveItem().split("\\*");
			listenerPacket.eventType = messageListener.getEventType();
			listenerPackets.add(listenerPacket);
		}
		/** 移除 监听器 */
		public void removeListener(MessageListener messageListener){
			for(int i=listenerPackets.size()-1;i>=0;i--){
				if (messageListener.hashCode() == listenerPackets.get(i).messageListener.hashCode()){
					listenerPackets.remove(i);
					break;
				}
			}
		}
		/** 清除监听器 */
		public void clearListener(){
			listenerPackets.clear();
		}

		/**
		 * 新增
		 * @param key
		 * @param item
		 * @param value
		 */
		public void setEvent(Object key, Object item,Object value){
			for(int i=0; i<listenerPackets.size();i++){
				if (listenerPackets.get(i).eventType == EventType.SET){
					if (this.validKey(listenerPackets.get(i).subKeys,key.toString(),listenerPackets.get(i).asterisk)){
						if (item == null || this.validKey(listenerPackets.get(i).subItems,item.toString(),listenerPackets.get(i).asterisk)){
							if (value != null)
								listenerPackets.get(i).messageListener.onMessage("SET",key,item, ObjectToByteUtils.toByteArray(value));
							else
								listenerPackets.get(i).messageListener.onMessage("SET",key,item,null);
						}
					}
				}
			}
		}

		/**
		 * 移除
		 * @param key
		 * @param item
		 */
		public void removeEvent(Object key, Object item){
			for(int i=0; i<listenerPackets.size();i++){
				if (listenerPackets.get(i).eventType == EventType.REMOVE){
					if (this.validKey(listenerPackets.get(i).subKeys,key.toString(),listenerPackets.get(i).asterisk)){
						if (item == null || this.validKey(listenerPackets.get(i).subItems,item.toString(),listenerPackets.get(i).asterisk)){
							listenerPackets.get(i).messageListener.onMessage("REMOVE",key,item, null);
						}
					}
				}
			}
		}



		/**
		 * 过期
		 * @param key
		 * @param item
		 * @param value
		 */
		public void ExpireEvent(Object key, Object item,Object value){
			for(int i=0; i<listenerPackets.size();i++){
				if (listenerPackets.get(i).eventType == EventType.EXPIRE){
					if (this.validKey(listenerPackets.get(i).subKeys,key.toString(),listenerPackets.get(i).asterisk)){
						if (item == null || this.validKey(listenerPackets.get(i).subItems,item.toString(),listenerPackets.get(i).asterisk)){
							if (value != null)
								listenerPackets.get(i).messageListener.onMessage("EXPIRE",key,item, ObjectToByteUtils.toByteArray(value));
							else
								listenerPackets.get(i).messageListener.onMessage("EXPIRE",key,item, null);
						}
					}
				}
			}
		}

		/**
		 * 查检带 * 号键是否一致
		 * @param subs
		 * @param orignKey
		 * @return
		 */
		public boolean validKey(String[] subs,String orignKey,boolean asterisk){
			/** 判断带 * 号的模糊查询 */
			if (subs==null){return true;}
			/** 带 * 模糊匹配 */
			if (asterisk == true){
				int begin =0;
				for(int i=0;i< subs.length;i++){
					begin = orignKey.indexOf(subs[i]);
					if (begin == -1){return false;}
					if (subs.length == 1 && begin != 0){
						/** 不带星号，又不是从左开始排除，则不成立 */
						return false;
					}
					orignKey = orignKey.substring(begin+subs[i].length(),orignKey.length());
				}
				return true;
			}
			/** 不带 * 全匹配 */
			for(String sub: subs){
				if (sub.equals(orignKey))
					return true;
			}
			return false;
		}

		public final class ListenerPacket{
			public AbstractMessageListener messageListener;
			public String[] subKeys;
			public String[] subItems;
			public EventType eventType;
			/** 是否带星号 */
			public boolean asterisk=false;
		}

	}
}
