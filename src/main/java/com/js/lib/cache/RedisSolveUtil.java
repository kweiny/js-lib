package com.js.lib.cache;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson2.JSONObject;

/**
 *
 * @类名: RedisSolveUtil
 * @说明: Redis操作类
 *
 * @author: kenny
 * @Date	2017年11月24日下午5:53:18
 * 修改记录：
 *
 * @see
 */
@Slf4j
@Component
public class RedisSolveUtil{
	private final static String UN_OVERDUE_KEY="UN-OVERDUE";

	@Autowired(required=false)
	private RedisTemplateUtil redisTemplateUtil;

	@SuppressWarnings("unused")
	private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();


	/**
	 * 缓存值设置
	 * @param key 键
	 * @param value  值可以是对象,map,list,set
	 * @param time  失效时间（0），0或null表示就改生效
	 */
	@SuppressWarnings("deprecation")
	public void set(String key, Object value, Long time) {
		String key1=key+"_type";
		if (time == null) time = 0L;

		if(value instanceof Map){ //是否为MAP数据
			redisTemplateUtil.set(  key1, "map",time);
			redisTemplateUtil.hmset(  key, (Map)value, time);
		}else if (value instanceof List){ //是否为list数据
			List<Object> value1 = (List<Object>)value;
			redisTemplateUtil.set(  key1, "list",time);
			redisTemplateUtil.lSet(  key, value1, time);
		}else if (value instanceof Set){
			redisTemplateUtil.set(  key1, "set",time);
			redisTemplateUtil.sSet(  key, time, (Object[])value);
		}else{
			redisTemplateUtil.set(  key, value,time);
		}

		//释放会话连接，启动事务以后必须手释放
		releaseConn();
	}


	/**
	 * 缓存值获取
	 * @param key
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public Object get(String key) {
		String key1=key+"_type";
		Object keyType = redisTemplateUtil.get(  key1);
		if (keyType == null){
			return redisTemplateUtil.get(  key);
		}
		if (keyType.equals("map")){
			return redisTemplateUtil.hmget(  key);
		}else if (keyType.equals("list")){
			Object obj = redisTemplateUtil.lGet(  key,0,-1);
			if (obj == null) return null;
			List<Object> objList = (List<Object>)obj;
			return objList;

		}else if (keyType.equals("set")){
			return redisTemplateUtil.sGet(  key);
		}
		//释放会话连接，启动事务以后必须手释放
		releaseConn();
		return null;
	}

	/**
     * 删除缓存
     * @param key
     */
	public void  del(String key) {
		String key1=key+"_type";
		if (redisTemplateUtil.get(key1) != null){
			redisTemplateUtil.del(key1);
		}
		redisTemplateUtil.del(key);

		//释放会话连接，启动事务以后必须手释放
		releaseConn();
	}


	/**
	 * 指定缓存失效时间
	 * @param key   key键
	 * @param time   时间(秒)
	 */
	public boolean setExpire(String key,long time){
		return redisTemplateUtil.expire(key, time);
	}
	public boolean setExpire(String key,String item,long time){
		if (StringUtils.isEmpty(item)){
			return this.setExpire(key,time);
		}
		return setOverdueKey(key,item,time);
	}



	/**
	 * 根据key 获取过期时间
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
	 */
	public long getExpire(String key){
		long result = redisTemplateUtil.getExpire(key);
		//释放会话连接，启动事务以后必须手释放
		releaseConn();
		return result;
	}

	public long getExpire(String key,String item){
		long result = 0L;
		if (!StringUtils.isEmpty(item)){
			Double score = (redisTemplateUtil.zscore(
					UN_OVERDUE_KEY,
					getJsonVal(key,item)));
			result = score==null?0L:score.longValue();
		}else{
			redisTemplateUtil.getExpire(key);
		}
		//释放会话连接，启动事务以后必须手释放
		releaseConn();
		return result;
	}

	//日志类
	private void log(String methodName,Object key, Object value){
		String msg = "***"+methodName+": " ;
		if (key != null){
			msg = msg + key.toString();
		}
		if ((value != null) && (log.isDebugEnabled())){
			msg = msg + "=" + value;
		}
		if (log.isDebugEnabled())
			log.debug(msg);
		if (log.isInfoEnabled())
			log.debug(msg);
	}

    ///释放会话连接，启动事务以后必须手释放
	private void releaseConn(){

/*		redisTemplate.exec();
		RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
*/
	}

	/**
	 * 存入hash值
	 * @param key   键
	 * @param item  项
	 * @param value  值
	 * @return
	 */
	public boolean hset(String key,String item,Object value,Long time){
		return setHashValues(key,item,value,time);
	}

	/**
	 * 取hash值
	 * @param key 键
	 * @param item 项
	 * @return
	 */
	public Object hget(String key,String item){
		return redisTemplateUtil.hget(key, item);
	}

	/**
	 * 删除某个数据
	 * @param key
	 * @param item
	 */
	public void hdel(String key, Object... item){
		redisTemplateUtil.hdel(key, item);
	}

    /**
     * 取得key下的所有数据
     * @param key
     * @return
     */
    public Map<Object,Object> hmget(String key){
    	return redisTemplateUtil.hmget(key);
    }

    private boolean setHashValues(String key,String item,Object value,Long time){
    	boolean result = false;
    	if (!StringUtils.isEmpty(item)){
    		/** 保存值 */
    		result = redisTemplateUtil.hset(key, item, value);

    		/** 保存 过期值 */
    		setOverdueKey(key,item,time);
    	}
    	return result;
    }

	/**
	 * 增加有序集合
	 * @param key
	 * @param value
	 * @param score
	 * @param time
	 * @return
	 */
	public boolean zset(String key,Object value,double score, long time){
		try{
			time = System.currentTimeMillis() + time*1000L;
			redisTemplateUtil.zset(key,value,score);
			/** 保存 过期值 */
			if (time != 0){
				/** value 是有序集合*/
				time = System.currentTimeMillis() + time*1000L;
				JSONObject data = new JSONObject();
				data.put("key",key);
				data.put("score",time);
				redisTemplateUtil.zset(
	    				UN_OVERDUE_KEY,
	    				data.toString(),
	    				time);
			}
	    	return true;
		}catch(Exception e){
			log.error(">>>>>>>>>> " + e.toString());
			return false;
		}
	}

	/**
	 * 读取有序集合
	 * @param key
	 * @param item
	 * @param from
	 * @param to
	 * @return
	 */
	public Set<Object> zget(String key,String item,double from,double to){
		try{
        	Set<Object> list= redisTemplateUtil.zget(key,from,to);
        	return list;
		}catch(Exception e){
			log.error(">>>>>>>>>> " + e.toString());
			return null;
		}
	}

	/**
	 * 删除有序集合
	 * @param key
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean zdel(String key,double from,double to){
		try{
			redisTemplateUtil.zdel(key, from, to);
        	return true;
		}catch(Exception e){
			log.error(">>>>>>>>>> " + e.toString());
			return false;
		}
	}


	public void zAdd(Object key, Object value, double score) {
		redisTemplateUtil.getRedisTemplate().opsForZSet().add(key, value, score);
	}
	public void zRemove(Object key, Object value) {
		redisTemplateUtil.getRedisTemplate().opsForZSet().remove(key, value);
	}
	public Double zIncrScore(Object key, Object value, double delta) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().incrementScore(key, value, delta);
	}
	public Double zScore(Object key, Object value) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().score(key, value);
	}
	public Long zRank(Object key, Object value) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().rank(key, value);
	}
	public Long zSize(Object key) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().zCard(key);
	}
	public Set<Object> zRange(Object key, int start, int end) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().range(key, start, end);
	}
	public Set<Object> zRevRange(Object key, int start, int end) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().reverseRange(key, start, end);
	}
	public Set<Object> zSortRange(Object key, int min, int max) {
		return redisTemplateUtil.getRedisTemplate().opsForZSet().rangeByScore(key, min, max);
	}

    /**
     * 执行删除过期数据
     */
    public Object excluteExpire(){
    	double now = System.currentTimeMillis();
    	/** 找出所有 有过期控制 的 key */
    	Set<Object> list= redisTemplateUtil.zget(
				UN_OVERDUE_KEY,
				1,
				now);
    	if (CollectionUtils.isEmpty(list)){
    		return null;
    	}

    	for (Object o : list){
    		/** 删除过期的 key-item */
    		log.debug("Redis KeyItem: {}",o);
    		JSONObject keyItem = JSONObject.parseObject(o.toString(), JSONObject.class);
    		if (keyItem.get("score") == null){
    			this.hdel(keyItem.get("key").toString(),keyItem.get("item"));
    		}else {
    			/** /** value 是有序集合 ；有序集合删除   */
    			redisTemplateUtil.zdel(keyItem.get("key").toString(),
    					Double.valueOf(keyItem.get("score").toString()),
    					Double.valueOf(keyItem.get("score").toString()));
    		}
    	}
    	/** 删除 有序集合 */
    	redisTemplateUtil.zdel(UN_OVERDUE_KEY, 1, now);

    	return list;
    }
    /**
     * 组装 key 和 item
     * @param key
     * @param item
     * @return
     */
    private String getJsonVal(String key,String item){
    	JSONObject keyItem = new JSONObject();
		keyItem.put("key", key);
		keyItem.put("item",item);
		return keyItem.toString();
    }

    /**
     * 设置过期时间
     * @param key
     * @param item
     * @param time
     */
    private boolean setOverdueKey(String key,String item,Long time){
    	try{
        	/** 保存 过期值 */
        	if (!StringUtils.isEmpty(item) && time != null && time != 0){
        		redisTemplateUtil.zset(
        				UN_OVERDUE_KEY,
        				getJsonVal(key,item),
        				System.currentTimeMillis() + time*1000L);
        	}
        	return true;
    	}catch(Exception e){
    		log.error(">>>>>>>>>> " + e.toString());
    		return false;
    	}

    }
	/**
	 * 模糊查询
	 * @param query 查询参数
	 * @return
	 */
	public List<Object> scan(String query) {
		return redisTemplateUtil.scan(query,100);

/*

		ScanOptions scanOptions = ScanOptions.scanOptions().match(query).count(1000).build();
		RedisSerializer<String> redisSerializer = (RedisSerializer<String>) stringRedisTemplate.getKeySerializer();
		return (Cursor) stringRedisTemplate.executeWithStickyConnection((RedisCallback) redisConnection ->
				new ConvertingCursor<>(redisConnection.scan(scanOptions), redisSerializer::deserialize));

*//*		private static Cursor<String> scan(StringRedisTemplate stringRedisTemplate, String match, int count){
			ScanOptions scanOptions = ScanOptions.scanOptions().match(match).count(count).build();
			RedisSerializer<String> redisSerializer = (RedisSerializer<String>) stringRedisTemplate.getKeySerializer();
			return (Cursor) stringRedisTemplate.executeWithStickyConnection((RedisCallback) redisConnection ->
					new ConvertingCursor<>(redisConnection.scan(scanOptions), redisSerializer::deserialize));
		}*//*


		Set<String> keys = (Set<String>) redisTemplate.execute((RedisCallback<Set<String>>) connection -> {
			Set<String> keysTmp = new HashSet<>();
			Cursor<byte[]> cursor = connection.scan(new ScanOptions.ScanOptionsBuilder().match(query).count(1000).build());
			while (cursor.hasNext()) {
				keysTmp.add(new String(cursor.next()));
			}
			return keysTmp;
		});


		return new ArrayList<>(keys);*/

	}

	public boolean hasKey(String key){
		return redisTemplateUtil.hasKey(key);
	}

    public boolean testZset(String key,String item,Long time){
    	return redisTemplateUtil.zset(
 				UN_OVERDUE_KEY,
				getJsonVal(key,item),
				time);
    }
}
