package com.js.lib.cache.listener;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;

/**
 * 当吸发生变化时
 *
 */
@Slf4j
@Data
@Accessors(chain = true)
public abstract class AbstractMessageListener implements MessageListener{
    /** 处理的key, 可带*号 */
    private String resolveKey="*";
    /** 处理的与key配合, 可带*号 */
    private String resolveItem="*";
    /** 类型 */
    private EventType eventType=EventType.SET;


    public void onMessage(String event,Object key, Object item,@Nullable byte[] value){
        log.debug(">>>>>>>>{} Key: {}",event,key);
        log.debug(">>>>>>>>{} item: {}",event,item);
        if (value != null)
            log.debug(">>>>>>>>{} item: {}",event,new String(value));
    }
}
