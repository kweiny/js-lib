package com.js.lib.cache.listener;

import org.springframework.lang.Nullable;

/**
 * 缓存消息处理接口
 */
public interface MessageListener {
    void onMessage(String event,Object var1, @Nullable Object var2,@Nullable byte[] var3);
}
