package com.js.lib.cache.listener;

/**
 * 监听事件描述
 */
public enum EventType {
    /**
     * 新增
     */
    SET,
    /**
     * 移除
     */
    REMOVE,
    /**
     * 过期
     */
    EXPIRE
}
