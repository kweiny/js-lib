package com.js.lib.cache;

import com.js.lib.cache.listener.AbstractMessageListener;
import com.js.lib.cache.listener.MessageListener;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 说明：
 * @类名: Cache
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2022年1月11日下午2:03:08
 */
public interface Cache {

	// local, redis
	String getCacheType();

	Object get(String key);
	void set(String key, Object object);

	/**
	 * 查询缓存数据，redis请用工具查询
	 * @return
	 */
	Map<Object,Object> viewData();
	/**
	 * 加入监听器, 暂不处理redis
	 * @param messageListener
	 */
	void addListener(AbstractMessageListener messageListener);

	/**
	 * 移除监听器 暂不处理redis
	 * @param messageListener
	 */
	void removeListener(AbstractMessageListener messageListener);

	/**
	 * 清除监听器 暂不处理redis
	 */
	void clearListener();

	/**
	 * 设置HASH MAP 数据
	 * @param key  键
	 * @param object 值
	 * @param time 过期(秒)
	 */
	void set(String key, Object object,Long time);
	void remove(String key);
	Boolean del(String key);

	/**
	 * @param key
	 * @param item
	 * @param value
	 * @param time 秒
	 */
	void hset(String key, String item,Object value, Long time);
	void hset(String key, String item,Object value);

	Object hget(String key, String item);
	Map<Object,Object> hmget(String key);

	void hdel(String key, Object... items);

	/**
	 * 返微 秒
 	 * @param key
	 * @param item
	 * @param time
	 * @return
	 */
	Long setExpire(String key,String item,Long time);

	void clearExpireData();

	Object showMaps(String key);
	Object showExpireData();

	/**
	 * 定时清理过期键
	 * @param interval 间隔时间 单位秒
	 */
	void autoClearExpire(Long interval);

	/**
	 *模糊查询
	 */
	List<Object> scan(String key);

	Boolean hasKey(String key);
	/** ====================================有序集合ZSet================================================= *//*
	/**
	 * 添加一个元素, zset与set最大的区别就是每个元素都有一个score，因此有个排序的辅助功能;  zadd
	 * @param key
	 * @param value
	 * @param score
	 */
	void zAdd(Object key, Object value, double score);

	/**
	 * 删除元素 zrem
	 * @param key
	 * @param value
	 */
	void zRemove(Object key, Object value);

	/**
	 * score的增加or减少 zincrby
	 *
	 * @param key
	 * @param value
	 * @param delta -1 表示减 1 表示加1
	 */
	Double zIncrScore(Object key, Object value, double delta);

	/**
	 * 查询value对应的score   zscore
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	Double zScore(Object key, Object value);


	/**
	 * 判断value在zset中的排名  zrank
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	Long zRank(Object key, Object value);

	/**
	 * 返回集合的长度
	 *
	 * @param key
	 * @return
	 */
	Long zSize(Object key);

	/**
	 * 查询集合中指定顺序的值， 0 -1 表示获取全部的集合内容  zrange
	 *
	 * 返回有序的集合，score小的在前面
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<Object> zRange(Object key, int start, int end);

	/**
	 * 查询集合中指定顺序的值  zrevrange
	 * 倒序排返回有序的集合中，score大的在前面
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<Object> zRevRange(Object key, int start, int end);
	/**
	 * 根据score的值，来获取满足条件的集合  zrangebyscore
	 *
	 * @param key
	 * @param min
	 * @param max
	 * @return
	 */
	Set<Object> zSortRange(Object key, int min, int max);

}
