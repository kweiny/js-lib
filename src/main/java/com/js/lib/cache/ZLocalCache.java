package com.js.lib.cache;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Slf4j
public class ZLocalCache {
    private static ZLocalCache instance;
    private ConcurrentHashMap<Object,Object> mainMap = new ConcurrentHashMap<Object,Object>();

    @PostConstruct
    public void ZLocalCache(){
        instance = this;
    }
    public static synchronized ZLocalCache getInstance() {
        if (instance == null) {
            instance = new ZLocalCache();
        }
        return instance;
    }

    /** ====================================有序集合ZSet================================================= */
    private ZKeyPacket createKeyPacket(Object key){
        ZKeyPacket keyPacket=null;
        Object obj = mainMap.get(key);
        if (obj == null){
            keyPacket = new ZKeyPacket(key);
            mainMap.put(key,keyPacket);
        }else{
            keyPacket = (ZKeyPacket)obj;
        }
        return keyPacket;
    }

    private ZKeyPacket getKeyPacket(Object key){
        Object obj = mainMap.get(key);
        if (obj == null) {return null;}
        return (ZKeyPacket) obj;

    }


    public Map<Object,Object> viewData(){
        Map<Object,Object> result = null;
        for(Map.Entry<Object,Object> entry : mainMap.entrySet()){
            if (result == null){
                result = new HashMap<>();
            }
            result.put(entry.getKey(),((ZKeyPacket)entry.getValue()) .view());
        }
        return result;
    }
    /**
     * 添加一个元素, zset与set最大的区别就是每个元素都有一个score，因此有个排序的辅助功能;  zadd
     *
     * @param key
     * @param value
     * @param score
     */
    public void zAdd(Object key, Object value, double score) {
        this.createKeyPacket(key).add(value,score);
    }

    /**
     * 删除元素 zrem
     * @param key
     * @param value
     */
    public void zRemove(Object key, Object value) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return;}
        if (keyPacket.remove(value)==0){
            mainMap.remove(key);
        }

    }

    /**
     * score的增加or减少 zincrby
     *
     * @param key
     * @param value
     * @param delta -1 表示减 1 表示加1
     */
    public Double zIncrScore(Object key, Object value, double delta) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}
        return keyPacket.IncrScore(value,delta);
    }

    /**
     * 查询value对应的score   zscore
     *
     * @param key
     * @param value
     * @return
     */
    public Double zScore(Object key, Object value) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}
        return keyPacket.zScore(value);
    }


    /**
     * 判断value在zset中的排名  zrank
     *
     * @param key
     * @param value
     * @return
     */
    public Long zRank(Object key, Object value) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}
        return keyPacket.zRank(value);
    }

    /**
     * 返回集合的长度
     *
     * @param key
     * @return
     */
    public Long zSize(Object key) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}
        return keyPacket.zSize();
    }

    /**
     * 查询集合中指定顺序的值， 0 -1 表示获取全部的集合内容  zrange
     *
     * 返回有序的集合，score小的在前面
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Set<Object> zRange(Object key, int start, int end) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}

        return keyPacket.ZRange(start,end);
    }
    /**
     * 查询集合中指定顺序的值和score，0, -1 表示获取全部的集合内容
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
/*    public Set<ZSetOperations.TypedTuple<String>> zRangeWithScore(Object key, int start, int end) {
        return redisTemplate.opsForZSet().rangeWithScores(key, start, end);
    }*/
    /**
     * 查询集合中指定顺序的值  zrevrange
     * 倒序排返回有序的集合中，score大的在前面
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Set<Object> zRevRange(Object key, int start, int end) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}

        return keyPacket.zRevRange(start,end);
    }
    /**
     * 根据score的值，来获取满足条件的集合  zrangebyscore
     *
     * @param key
     * @param min
     * @param max
     * @return
     */
    public Set<Object> zSortRange(Object key, int min, int max) {
        ZKeyPacket keyPacket = this.getKeyPacket(key);
        if (keyPacket == null){return null;}
        return keyPacket.ZRange(min,max);
    }

    public ConcurrentHashMap<Object, Object> getMainMap() {
        return mainMap;
    }

    /** 存储的数据结构类 */
    public final class KeyPacket{
        /** 真实数据 */
        private Object key;
        private ConcurrentHashMap<Object,Object> valueScoreMap = new ConcurrentHashMap<Object,Object>();
        private ConcurrentSkipListMap<Object, List<Object>> zsetMap = new ConcurrentSkipListMap<Object,List<Object>>();

        public KeyPacket(){}
        public KeyPacket(Object key){this.key=key;};

        public boolean isEmpty(){
            return valueScoreMap.size() <= 0;
        }

        /**
         * 增加新的
         * @param value
         * @param score
         */
        public void add(Object value,double score){
            valueScoreMap.put(value.hashCode(),score);
            List<Object> valueList = null;
            Object obj=zsetMap.get(score);
            if (obj == null){
                valueList = new ArrayList<>();
            }else{
                valueList = (List<Object>)obj;
            }
            valueList.add(value);
            zsetMap.put(score,valueList);
            log.debug(String.format("add: %s,%s",value,score));
        }

        /**
         * 删除
         * @param value
         * @return
         */
        public int remove(Object value){
            int reesult = -1;
            /** 取得score */
            Object score=valueScoreMap.get(value.hashCode());
            if (score == null){return reesult;}
            valueScoreMap.remove(value.hashCode());

            /** 取得之前保存的 value */
            this.removeValueFromList(score,value);
            if (valueScoreMap.size() == 0){
                valueScoreMap = null;
                return -1;
            }
            log.debug(String.format("remove: %s",value));
            return valueScoreMap.size();
        }

        /**
         * 修改 score
         * @param value
         * @param delta
         * @return
         */
        public double IncrScore(Object value, double delta){
            /** 取得源score */
            Object score=valueScoreMap.get(value.hashCode());
            if (score == null){return 0;}

            this.removeValueFromList(score,value);

            double newScore = Double.valueOf(score.toString());
            newScore = newScore + delta;

            this.addValueToList(newScore,value);
            log.debug(String.format("IncrScore: %s,%s",value,delta));
            return  newScore;
        }

        /**
         * 查询value对应的score   zscore
         * @param value
         * @return
         */
        public Double zScore(Object value) {
            Object obj = valueScoreMap.get(value.hashCode());
            if (obj == null){return 0.0;}
            return Double.valueOf(obj.toString());
        }

        /**
         * 判断value在zset中的排名  zrank
         * @param value
         * @return
         */
        public Long zRank(Object value) {
            Object obj = valueScoreMap.get(value.hashCode());
            if (obj == null){return -1L;}
            double to = Double.valueOf(obj.toString());
            Long order=0L;
            ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(0.0,to);
            for (Map.Entry<Object,List<Object>> entry : subMap.entrySet()) { //取一定范围的集合
                if (Double.valueOf(entry.getKey().toString())< to){
                    order = order + entry.getValue().size();
                }else{
                    for(Object val : entry.getValue()){
                        order++;
                        if (val.hashCode() >= val.hashCode()){
                            return order;
                        }
                    }
                }

            }
            return order;
        }

        /**
         * 返回集合的长度
         * @return
         */
        public Long zSize() {
            return Long.valueOf(valueScoreMap.size());
        }
        /**
         * 查询集合中指定顺序的值， 0 -1 表示获取全部的集合内容  zrange
         * 返回有序的集合，score小的在前面
         * @param start
         * @param end
         * @return
         */
        public Set<Object> ZRange(int start, int end) {
            double dStart = Double.valueOf(start);
            double dEnd = Double.valueOf(end);
            /** LinkedHashSet 可以按写入顺序 来输出 */
            Set<Object> result = new LinkedHashSet<Object>();

            if (start == 0 && end == -1){
                /** 全部 */
                for (Map.Entry<Object,List<Object>> entry : zsetMap.entrySet()) { //取一定范围的集合
                    result.addAll(entry.getValue());
                }
            }else{
                ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(dStart,dEnd);
                for (Map.Entry<Object,List<Object>> entry : subMap.entrySet()) { //取一定范围的集合
                    result.addAll(entry.getValue());
                }
            }
            return result;
        }

        /**
         * 查询集合中指定顺序的值  zrevrange
         * 倒序排,返回有序的集合中，score大的在前面
         * @param start
         * @param end
         * @return
         */
        public Set<Object> zRevRange(int start, int end) {
            double dStart = Double.valueOf(start);
            double dEnd = Double.valueOf(end);

            Set<Object> result = new LinkedHashSet<>();
            if (start == 0 && end == -1){
                /** 全部 */
                ConcurrentNavigableMap<Object, List<Object>> subMap1 = zsetMap.descendingMap();
                for (Map.Entry<Object,List<Object>> entry : subMap1.entrySet()) { //取一定范围的集合
                    result.addAll(entry.getValue());
                }
            }else {

                /** 选择出范围 */
                ConcurrentNavigableMap<Object, List<Object>> subMap = zsetMap.subMap(dStart, dEnd);
                /** 倒序排 */
                ConcurrentNavigableMap<Object, List<Object>> subMap1 = subMap.descendingMap();
                for (Map.Entry<Object, List<Object>> entry : subMap1.entrySet()) { //取一定范围的集合
                    result.addAll(entry.getValue());
                }
            }


            System.err.println(result.toString());

            return result;
        }

        /**
         * List 中移除 Value
          * @param score
         * @param value
         */
        private void removeValueFromList(Object score,Object value){
            /** 取得之前保存的 value */
            Object obj = zsetMap.get(score);
            if (obj == null){return;}
            List<Object> valueList = (List<Object>)obj;
            valueList.remove(value);
            if (valueList.size()==0){
                zsetMap.remove(score);
            }
        }

        /**
         * 加 Value 到新的List 中
         * @param score
         * @param value
         */
        private void addValueToList(Object score,Object value){
            Object obj = zsetMap.get(score);
            if (obj == null){return;}
            List<Object> valueList = (List<Object>)obj;
            valueList.add(value);
        }
    }

}
