package com.js.lib.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.js.lib.cache.listener.AbstractMessageListener;
import com.js.lib.cache.listener.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 说明：
 * @类名: RedisCache
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2022年1月11日下午1:36:42
 */
@Slf4j
@Service("redisCache")
public class RedisCache implements Cache{
	private static RedisCache instance;

	@Autowired
	private RedisSolveUtil redisUtil;

	@Override
	public String getCacheType(){
		return "redis";
	}

	@Override
	public Map<Object,Object> viewData(){
		Map<Object,Object> map = new HashMap<>();
		map.put("main","请用redis工具查询");
		return map;
	}
	@Override
	public Object get(String key) {return getValue(key,null);}

	@Override
	public void set(String key, Object object) {setValue(key,null,object,null);}

	@Override
	public void addListener(AbstractMessageListener messageListener) {
		log.info(">>>>>>>>>> 启用redis 不在这里处理：addListener");
	}

	@Override
	public void removeListener(AbstractMessageListener messageListener) {
		log.info(">>>>>>>>>> 启用redis 不在这里处理：removeListener");
	}

	@Override
	public void clearListener() {
		log.info(">>>>>>>>>> 启用redis 不在这里处理：clearListener");
	}

	@Override
	public void set(String key, Object object, Long time) {setValue(key,null,object,time);}

	@Override
	public void remove(String key) {redisUtil.del(key);}

	@Override
	public Boolean del(String key) {
		this.remove(key);
		return true;
	}

	@Override
	public void hset(String key, String item, Object value, Long time){setValue(key,item,value,time);}

	@Override
	public void hset(String key, String item, Object value) {setValue(key,item,value,null);}

	@Override
	public Object hget(String key, String item) {return getValue(key,item);}

	@Override
	public Map<Object, Object> hmget(String key) {return redisUtil.hmget(key);}

	@Override
	public void hdel(String key, Object... items) {redisUtil.hdel(key,items);}

	@Override
	public Long setExpire(String key, String item, Long time) {redisUtil.setExpire(key,item,time);return 0L;}

	@Override
	public void clearExpireData() {
		redisUtil.excluteExpire();
	}

	@Override
	public Object showMaps(String key) {
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("result", "请使用Redis工具查看");
		return result;
	}

	@Override
	public Object showExpireData() {
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("result", "请使用Redis工具查看");
		return result;
	}

	@Override
	public void autoClearExpire(Long interval) {
		log.info("Redis 不支持清除过期数据的功能 ");
	}



	private Object getValue(String key,String item){
		Object result=null;
		if (item == null){result  = redisUtil.get(key);}else{result = redisUtil.hget(key,item);}
		return result;
	}
	private void setValue(String key,String item,Object value,Long time){
		if (StringUtils.isEmpty(item)){redisUtil.set(key, value, time);}else{redisUtil.hset(key,item,value,time);}
	}

	/**
	 * 模糊查询
	 * @param key
	 * @return
	 */
	@Override
	public List<Object> scan(String key) {
		return redisUtil.scan(key);
	}

	@Override
	public Boolean hasKey(String key) {
		try {
			return redisUtil.hasKey(key);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void zAdd(Object key, Object value, double score) {
		redisUtil.zAdd(key, value, score);
	}

	@Override
	public void zRemove(Object key, Object value) {
		redisUtil.zRemove(key, value);
	}

	@Override
	public Double zIncrScore(Object key, Object value, double delta) {
		return redisUtil.zIncrScore(key, value, delta);
	}

	@Override
	public Double zScore(Object key, Object value) {
		return redisUtil.zScore(key, value);
	}

	@Override
	public Long zRank(Object key, Object value) {
		return redisUtil.zRank(key, value);
	}

	@Override
	public Long zSize(Object key) {
		return redisUtil.zSize(key);
	}

	@Override
	public Set<Object> zRange(Object key, int start, int end) {
		return redisUtil.zRange(key, start, end);
	}

	@Override
	public Set<Object> zRevRange(Object key, int start, int end) {
		return redisUtil.zRevRange(key, start, end);
	}

	@Override
	public Set<Object> zSortRange(Object key, int min, int max) {
		return redisUtil.zSortRange(key, min, max);
	}
}
