package com.js.lib.exception;

import lombok.Data;

/**
 * 说明：自定义异常类
 * @类名: CustomException
 *
 * @author   kenny
 * @Date	 2020年1月7日下午2:16:47
 */
@Data
public class CustomException extends RuntimeException{

	/** */
	private static final long serialVersionUID = -849559568033167772L;
	private Integer code;
	private String msg;
	/** 附加信息*/
	private String[] extendMsg;

    public CustomException(Integer code,String msg,String ... extendMsg) {
    	super(msg);
    	this.code = code;
    	this.msg = msg;
    	this.extendMsg = extendMsg;
    }
}
