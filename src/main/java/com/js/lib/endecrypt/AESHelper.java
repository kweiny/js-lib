package com.js.lib.endecrypt;

import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

/**
 * 说明：AES加密解密
 * @类名: AesUtil
 *
 * @author   kenny
 * @Date	 2020年7月10日下午6:21:56
 */
public class AESHelper {

	public static final String AES_TYPE = "AES/ECB/PKCS5Padding";

	private static String AES_KEY = null;
    /**
     * 加密
     * 加密方式： AES128(CBC/PKCS5Padding) + Base64
     * @param cleartext
     * @return
     */
    public static String encrypt(String cleartext,String aesKey) {
        try {
            //私钥字节数组,加密方式 AES或者DES
            SecretKeySpec key = new SecretKeySpec(aesKey.getBytes(), "AES");
            //实例化加密类，参数为加密方式，要写全
            Cipher cipher = Cipher.getInstance(AES_TYPE);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptedData = cipher.doFinal(cleartext.getBytes("UTF-8"));
            //用BASE64 编码，还可以用HEX, UUE,7bit等
            //return Base64.encodeBase64String(encryptedData);

            //注意：JDK9 之后不支持这种写法，需要使用Base64.Encoder encoder=Base64.getEncoder();
            BASE64Encoder encoder=new BASE64Encoder();
            return encoder.encode(encryptedData);

            //return Base64.encodeBase64(encryptedData);
            //return new BASE64Encoder().encode(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 先用Base64 解码
     *
     *
     * @param encrypted
     * @return
     */
    public static String decrypt(String encrypted,String aesKey) {
        try {
            //byte[] byteMi = new BASE64Decoder().decodeBuffer(encrypted);
        	//byte[] byteMi = Base64.decodeBase64(encrypted);

            BASE64Decoder decoder=new BASE64Decoder();
            byte[] byteMi = decoder.decodeBuffer(encrypted);


            SecretKeySpec key = new SecretKeySpec(aesKey.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance(AES_TYPE);
             cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decryptedData = cipher.doFinal(byteMi);
            return new String(decryptedData, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    /**
     * 产生aeskey
     * @return
     */
    public static String genrateKey() {
    	AES_KEY = MD5.getMD5Str(new Date().toString()).substring(0,16);
    	return AES_KEY;
    }


    public static String getAES_KEY() {
    	if (AES_KEY == null)
    		genrateKey();
		return AES_KEY;
	}


	public static void main(String[] args) throws Exception {
    	final String aesKey="8673db2f2af04cec";
    	String context = "这是一个AES加密解密测试程序..................哦哦哦.................";
    	System.out.println("这是明文: " + context);
    	String ciphertext=encrypt(context,aesKey);
    	System.out.println("这是密文: " + ciphertext);
    	//ciphertext="0N9QJL7mzbyBDzpDrSESDl/JGwGNbvgaqI4w9mCasUXiA/ig5rbLxuC4e7hKAM2C2pFHbC76fnBy3CNI0WsvdcHC6fhsU3AT9zEDqxawPknyJLQHvNrIiDLATcPECvtw";
    	ciphertext="DlAfsaeltXaZzJBg47RZ/K6TlKXl/PbB6Me9OToiZsviA/ig5rbLxuC4e7hKAM2C2pFHbC76fnBy3CNI0WsvdcHC6fhsU3AT9zEDqxawPknyJLQHvNrIiDLATcPECvtw";
    	//ciphertext="p6rDeke/7rFQjLa4AVQmFzBsm+sUPHBPu56/qFmwbierqfa53YI26MvV7XA0rQPdWW7eTz/VG2Gq\r\nTishi/EgEW6nxqLSjTdAMK24JPRWaQc=";
    	System.out.println("这是解密明文: " + decrypt(ciphertext,aesKey));
    }
}
