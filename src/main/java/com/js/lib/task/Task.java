package com.js.lib.task;

import java.util.concurrent.Future;

import com.js.lib.result.Result;

public interface Task<T> {
	public Future<String> execute(T t) throws Exception;
	
	public Future<T> execute1(T t) throws Exception;
	
	public Future<Result> execute2(T t) throws Exception;
}
