package com.js.lib.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.Data;

/**
 * redis配置
 * @author 10946
 *
 */
@Component("redisConfigProperties")
@ConfigurationProperties(prefix = "redis")
@Data
public class RedisConfigProperties {
	private boolean active=false;
	private int maxIdle=8;
    private int maxActive=300;
    private int maxWait=1000;
    private int timeout=100000;
    private int maxTotal=1000;
    private int minIdle=0;
    private int database=0;
    private boolean testOnBorrow=true;

	/** single单机模式，sentinel 哨兵模式， cluster 集群模式 */
	private String model = "single";

	private Single single;

	private Sentinel sentinel;

	private Cluster Cluster;

	@Data
	public static class Single{
		private String host;
	    private int port;
	    private String pwd;
	}

	@Data
	public static class Sentinel{
		private String masterName;
		private String password;
		private String nodes;
		private String node1;
		private String node2;
		private String node3;
	}

	@Data
	public static class Cluster{
		private String nodes;
		private int maxRedirects;
	}

}
