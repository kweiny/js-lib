package com.js.lib.result;

import lombok.Data;

/**
 * 说明：
 * @类名: ApiResult
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2021年12月7日上午10:46:11
 */
@Data
public class ApiResult<T> {
	private int code;
	private boolean success = true;
	private String msg;
	private T data;
	
	public ApiResult(){
		super();
	}

	public ApiResult(int code, boolean success, String msg, T data) {
		super();
		this.code = code;
		this.success = success;
		this.msg = msg;
		this.data = data;
	}
	public ApiResult(int code, boolean success, String msg) {
		this(code,success, msg, null);
	}
	
	public static ApiResult<?> ok(){
		return new ApiResult<Object>(0,true,"操作成功",null);
	}
	public static ApiResult<?> ok(String msg ){
		return new ApiResult<Object>(0,true,msg,null);
	}
	public static ApiResult<?> ok(int code,String msg ){
		return new ApiResult<Object>(code,true,msg,null);
	}
	public static ApiResult<?> ok(int code,String msg,Object obj ){
		return new ApiResult<Object>(code,true,msg,obj);
	}
	
	public static ApiResult<?> ok(Object obj ){
		return new ApiResult<Object>(0,true,"操作成功",obj);
	}
	public static ApiResult<?> ok(String msg,Object obj ){
		return new ApiResult<Object>(0,true,msg,obj);
	}
	
	public static ApiResult<?> error(){
		return new ApiResult<Object>(-1,false,"操作失败",null);
	}
	public static ApiResult<?> error(String msg){
		return new ApiResult<Object>(-1,false,msg,null);
	}
	
	public static ApiResult<?> error(Object obj){
		return new ApiResult<Object>(-1,false,"操作失败",obj);
	}
	public static ApiResult<?> error(String msg,Object obj){
		return new ApiResult<Object>(-1,false,msg,obj);
	}
	public static ApiResult<?> error(int code,String msg){
		return new ApiResult<Object>(code,false,msg,null);
	}
	public static ApiResult<?> error(int code,String msg,Object obj){
		return new ApiResult<Object>(code,false,msg,obj);
	}
}
