package com.js.lib.result;


import lombok.Data;

/**
 * @author Administrator
 *
 */
@Data
public class Result {

	private int code;
	private boolean success = true;
	private String msg;
	private Object data;
	
	public Result(){
		super();
	}
	public Result(int code, boolean success, String msg, Object data) {
		super();
		this.code = code;
		this.success = success;
		this.msg = msg;
		this.data = data;
	}
	public Result(int code, boolean success, String msg) {
		this(code,success, msg, null);
	}
	
	public static Result ok(){
		return new Result(0,true,"操作成功",null);
	}
	public static Result ok(String msg ){
		return new Result(0,true,msg,null);
	}
	public static Result ok(int code,String msg ){
		return new Result(code,true,msg,null);
	}
	public static Result ok(int code,String msg,Object obj ){
		return new Result(code,true,msg,obj);
	}
	
	public static Result ok(Object obj ){
		return new Result(0,true,"操作成功",obj);
	}
	public static Result ok(String msg,Object obj ){
		return new Result(0,true,msg,obj);
	}
	
	public static Result error(){
		return new Result(-1,false,"操作失败",null);
	}
	public static Result error(String msg){
		return new Result(-1,false,msg,null);
	}
	
	public static Result error(Object obj){
		return new Result(-1,false,"操作失败",obj);
	}
	public static Result error(String msg,Object obj){
		return new Result(-1,false,msg,obj);
	}
	public static Result error(int code,String msg){
		return new Result(code,false,msg,null);
	}
	public static Result error(int code,String msg,Object obj){
		return new Result(code,false,msg,obj);
	}
}
