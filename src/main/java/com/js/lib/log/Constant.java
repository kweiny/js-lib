package com.js.lib.log;
/**
 * 说明：日志常量
 * @类名: Constant
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2021年10月21日下午2:55:40
 */
public class Constant {

	/** 追溯码 */
	public static final String LOG_TRACE = "traceId";
}
