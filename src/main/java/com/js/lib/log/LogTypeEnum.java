package com.js.lib.log;
/**
 * 说明：API日志类型
 * @类名: LogTypeEnum
 *
 * @author   kenny
 * @Date	 2020年1月6日上午10:07:21
 */

public enum LogTypeEnum {

	 /** api调用前，带参数 */
	before {
	        @Override
	        public String toString() {return "调用前";}
	    },
	/** api调用后，返回结果 */
	after {
	        @Override
	        public String toString() {return "调用后";}
	    },

}
