package com.js.lib.log;


/**
 * 说明：日志接口
 * @类名: LogService
 *
 * @author   kenny
 * @Date	 2019年12月27日下午4:08:13
 */
public interface LogService {

	/**
	 * exception 为null值则用info输出, 否则用error输出
	 * @param obj 日志的内容
	 * @param exception 异常信息
	 */
	void log(Object obj,String exception);

}
