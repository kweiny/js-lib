package com.js.lib.log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.js.lib.utils.DateUtils;
import com.js.lib.utils.JsonUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 说明：日志
 * @类名: LoggerVO
 *
 * @author   kenny
 * @Date	 2020年1月6日上午9:51:22
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LoggerVO implements Serializable{
	private static final long serialVersionUID = 1485738228112310091L;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS",timezone="GMT+8")
	/** 时间 */
	private Date time;
	/** 客户端ip*/
	private String ip;
	/** 接口名*/
	private String apiName;
	/** url*/
	private String url;
	/** 调用接口前日志/调用接后日志*/
	private LogTypeEnum logType;
	/** 参数或结果数据*/
	private Object data;
	/** 追溯码 */
	private String tradeCode;
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n");
		sb.append("time: " +  DateUtils.dateTimeMillecondFormat(this.getTime()));
		sb.append("\r\n");
		sb.append("Tradecode: " + this.getTradeCode());
		sb.append("\r\n");
		sb.append("Client Ip: " + this.getIp());
		sb.append("\r\n");
		sb.append("Api Name: " + this.apiName);
		sb.append("\r\n");
		sb.append("Api Url: " + this.getUrl());
		sb.append("\r\n");
		sb.append("Before/After:  " + this.getLogType());
		sb.append("\r\n");
		
		if (this.data != null)
			sb.append("data: " + JsonUtil.toJson(this.data));
		sb.append("\r\n");
		return sb.toString();
	}
}
