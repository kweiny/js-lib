package com.js.lib.log;

import org.springframework.stereotype.Service;

import com.js.lib.utils.JsonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 说明：默认的日志保存到在文件上
 * @类名: DefaultLogServiceImpl
 *
 * @author   kenny
 * @Date	 2020年3月4日上午11:33:00
 */
@Service("defaultLogServiceImpl")
@Slf4j
public class DefaultLogServiceImpl implements LogService{

	@Override
	public void log(Object obj, String exception) {
		if (obj instanceof LoggerVO) {
			LoggerVO loggerVO  = (LoggerVO) obj;
			log(loggerVO.toString(),exception);
		}else{
			log(JsonUtil.toJson(obj),exception);
		}
	}
	
	private void log(String msg,String exception){
		if (exception == null){
			log.debug("日志信息：{}",msg);
		}else{
			log.error("日志信息：{},异常信息：{}",msg,exception);
		}
	}
	
}
