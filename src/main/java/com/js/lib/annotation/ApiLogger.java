package com.js.lib.annotation;

import java.lang.annotation.*;

/**
 * 说明：接口日志注解
 * @类名: Logger
 *
 * @author   kenny
 * @Date	 2019年12月31日下午2:49:18
 */
@Inherited
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLogger {
	String apiName() default "";
}
