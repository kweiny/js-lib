package com.js.lib.annotation.aop;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.*;

/**
 * 表属性说明， 实体类在生成文件
 */
@Inherited
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface TablePropertity {
    String apiName() default "";
    //

    /**Swwager 配置 @Api(value = "/user", tags = "用户管理")*/
    String swApiValue() default "";
    String swApiTags() default "";

    //@RequestMapping(value = "/user")
    String requestMapping() default "";

}
