package com.js.lib.annotation;

import java.lang.annotation.*;

/**
 * 限流器
 * @author kenny
 * @since 2019-09-19 11:56
 */
@Inherited
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimit {
	/**并发数*/
	int limiterNumber() default 300;
}
