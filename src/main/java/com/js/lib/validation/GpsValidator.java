package com.js.lib.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 说明：经纬度数据格式校验器
 * @类名: GpsValidator
 *
 * @author   kenny
 * @Date	 2020年1月4日上午11:17:32
 */
public class GpsValidator implements ConstraintValidator<GpsValid, Object>{

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		
		/** 这两行没设置一般会报 Unexpected exception during isValid call */
        if (value == null){return true;}
		if (StringUtils.isEmpty(value)){return true;}
		
		String str = value.toString();
		//格式1：带小数
    	String regEx1 = "^[+-]?\\d*\\.\\d*$";
    	
    	
    	boolean result = Pattern.compile(regEx1).matcher(str).matches();
    	
    	if (result == true){
    		;
    		//格式2：排除0.0 的去掉
    		//result = !Double.valueOf(str).equals(0.0);
    	}else{
    		System.out.println("GPS:" + str);
    	}
    	return result;
    	
/*    	boolean b2 = !Pattern.compile(regEx2).matcher(str).matches();
    	return Pattern.compile(regEx1).matcher(str).matches() && Pattern.compile(regEx2).matcher(str).matches();
*/    	
	}

}
