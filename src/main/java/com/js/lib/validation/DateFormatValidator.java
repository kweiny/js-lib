package com.js.lib.validation;

import java.util.Date;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.util.StringUtils;

import com.js.lib.utils.DateUtils;

/**
 * 说明：ISO日期时间类型的校验器
 * @类名: IsoDateFormatValidator
 *
 * @author   kenny
 * @Date	 2019年12月31日上午10:30:58
 */
public class DateFormatValidator implements ConstraintValidator<DateFormatValid, Object> {

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		/*
		 * ISO 8601 时期格式：
		 * 1、北京时间：2018-10-11T11:54:30Z 
		 * 2、UTC时间：2018-10-11T11:54:30+08:00
		 * 
		 * 暂只处理非ISO日期格式
		 */
		try{
			//禁止默认消息返回
			//context.disableDefaultConstraintViolation();
	        //自定义返回消息,{}号读取文件配置里的信息
	        //constraintValidatorContext.buildConstraintViolationWithTemplate("{custom.error}").addConstraintViolation();

	        if (value == null){return true;}
			if (StringUtils.isEmpty(value)){return true;}
			Date d = DateUtils.parseDateTime(value.toString());
		}catch(Exception e){
			return false;
		}
		return true;

	}

}
