package com.js.lib.validation;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.*;

/**
 * 说明：日期时间格式的校验注解
 * @类名: IsoDateFormatValid
 *
 * @author   kenny
 * @Date	 2019年12月31日上午10:30:58
 */

@Constraint(validatedBy = DateFormatValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})


public @interface DateFormatValid {
	String message() default "日期时间格式异常";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
    String dateFormat() default "yyyy-MM-dd HH:mm:ss";

}
