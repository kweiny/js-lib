package com.js.lib.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
/**
 * 说明：经纬度数据格式校验注解
 * @类名: GpsValid
 *
 * @author   kenny
 * @Date	 2020年1月4日上午11:14:41
 */
@Constraint(validatedBy = GpsValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface GpsValid {
	String message() default "GPS数据错误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
