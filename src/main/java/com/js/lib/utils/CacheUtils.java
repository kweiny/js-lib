package com.js.lib.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.js.lib.cache.SysCache;
import com.js.lib.device.RecorderForMemBean;
import com.js.lib.device.ResultGPSInfo;
/*import com.google.gson.Gson;
import com.js.devapi.cache.SysCache;
import com.js.devapi.controller.paramvo.HeartBeatVo;
import com.js.devapi.device.RecorderForMemBean;
import com.js.devapi.device.ResultGPSInfo;*/
/**
 * 
 * @类名: CacheUtils
 * @说明: 为简化操作，从缓存中查询、操作各类数据
 *
 * @author: kenny
 * @Date	2020年4月13日下午3:50:39
 * 修改记录：
 *
 * @see
 */
public class CacheUtils {
	/** 4G执法仪心跳参数 */
	private static final String DEVICE_HEARTBEAT="_device_heartbeat_";
	/** 设备在线，如果设备发送了心跳上来即在线，保存到此数据域中，设置过期时间默认1分钟 */
	public static final String DEVICE_ONLINE_TIME="_DEVICE_ONLINE_TIME"; 
	/** 实时定位  */
	public final static String VAR_CURRENT_GPS_POSITION="_current_gps_position";
	/**  保存登录用户的信息   */
	public final static String VAR_OPERATOR_LOGIN_MSG = "_operatorLoginMsg";
	/** 按通道号记录的设备列表 */
	public final static String VAR_RECORDER_ALARMCHANNEL_ITEM = "_var_recorder_alarmchannel_item";
	/** 按设备号记录的设备列表 */
	public final static String VAR_RECORDER_ITEM = "_var_recorder_item";
	
	/** 默认的在线设备 时间 2分钟*/
	private static Long onlineExpired = 120000L;
	
	/**
	 * 从缓存中取得 设备的封装类 RecorderForMemBean
	 * @param code 设备id或编号
	 * @return
	 */
	public static RecorderForMemBean getRecBean(String code){
		Object devObj = SysCache.getInstance().hget(VAR_RECORDER_ITEM,code);
		
		if (devObj != null){
			return  JsonUtil.fromJson((String) devObj, RecorderForMemBean.class);
		}
		return null;

	}
	
	/**
	 * 从缓存中取得 设备的封装类 RecorderForMemBean by AlarmChanne
	 * @param channel
	 * @return
	 */
	public static RecorderForMemBean getRecBeanByAlarmChannel(String channel){
		Object devObj = SysCache.getInstance().hget(VAR_RECORDER_ALARMCHANNEL_ITEM,channel);
		
		if (devObj != null){
			return  JsonUtil.fromJson((String) devObj, RecorderForMemBean.class);
		}
		return null;

	}

	
	/**
	 * 判断此设备是否存在
	 * @param code
	 * @return
	 */
	public static Boolean isExistsRecBean(String code){
		return getRecBean(code)==null?false:true;
	}
	
	/**
	 * 取得所有的 RecorderForMemBean
	 * @return
	 */
	public static List<RecorderForMemBean> getAllRecBean(){
		Object objList = SysCache.getInstance().hmget(VAR_RECORDER_ITEM);
		if (objList == null)
			return null;
		
		List<RecorderForMemBean> result=new ArrayList<RecorderForMemBean>();
		for (Object obj: result){
			result.add(JsonUtil.fromJson((String) obj, RecorderForMemBean.class));
		}
		return result;
	}
	
	/**
	 * 设置 RecorderForMemBean 到缓存
	 * @param rm
	 * @param seconds
	 */
	public static void setRecBean(RecorderForMemBean rm,Long seconds){
		SysCache.getInstance().hset(VAR_RECORDER_ITEM,
				rm.getCode(),JsonUtil.toJson(rm).toString(),
				0L);
	}

	/**
	 * 取当前定位信息
	 * @param deviceCode
	 * @return
	 */
	public static ResultGPSInfo getCurrGps(String deviceCode){
		Object obj =SysCache.getInstance().hget(VAR_CURRENT_GPS_POSITION,deviceCode);
		if (obj == null){return null;}
		else{return JsonUtil.fromJson(obj.toString(), ResultGPSInfo.class);}
	}
	

	/**
	 * 存当前定位信息
	 * String deviceCode 设备号
	 * @param resultGPSInfo gps数据对象
	 * @param timeOut 超时，单位 秒
	 * @return
	 */
	public static Boolean setCurrGps(String deviceCode,ResultGPSInfo resultGPSInfo,Long timeOut){
		try{
			timeOut = 0L;
			resultGPSInfo.setCurrTime(new Date().getTime());
			SysCache.getInstance().hset(VAR_CURRENT_GPS_POSITION,
					deviceCode,
					JsonUtil.toJson(resultGPSInfo),
					timeOut
					);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 是否在线
	 * @param deviceCode
	 * @return
	 */
	public static Boolean isOnline(String deviceCode){
		Object obj = SysCache.getInstance().hget(DEVICE_ONLINE_TIME,deviceCode);
		if(obj == null){
			return false;
		}
		return SysCache.getInstance().hget(DEVICE_ONLINE_TIME,deviceCode)!= null;
	}
	
	public static Boolean expireTime(String deviceCode,long milliseconds ){
		Object obj = SysCache.getInstance().hget(DEVICE_ONLINE_TIME,deviceCode);
		if(obj == null){
			return false;
		}
		long seconds = (long)obj;
		long times = (milliseconds - seconds ) / 1000 ;
		if(times >= 90){
			return false;
		}
		return true;
	}
	
	
	/** 
	 * 设备设备在线状态
	 * @param deviceCode 设备号
	 * @param timeOut 超时时间 秒
	 * @return
	 */
	public static void setOnline(String deviceCode,Long timeOut){
		SysCache.getInstance().hset(DEVICE_ONLINE_TIME,deviceCode,new Date().getTime(),timeOut);
	}
	/**
	 * 设备设备在线状态，默认的在线时间
	 * @param deviceCode
	 * @return
	 */
	public static void setOnline(String deviceCode){
		SysCache.getInstance().hset(DEVICE_ONLINE_TIME,deviceCode,new Date().getTime(),onlineExpired);
	}
	/**
	 * 设置设备离线
	 * @param deviceCode
	 */
	public static void setOffline(String deviceCode){
		SysCache.getInstance().hdel(DEVICE_ONLINE_TIME,deviceCode);
	}

	/**
	 * 取得所有在线的设备
	 * @return
	 */
	public static Map<Object,Object> getAllDeviceCodeWhenOnline(){
		return SysCache.getInstance().hmget(DEVICE_ONLINE_TIME);
	}
	
	/**
	 * 保存心跳信息
	 * @param hb
	 */
	public static void saveHeartbeat(String key,String hb){
		SysCache.getInstance().hset(DEVICE_HEARTBEAT, 
				//hb.getDeviceCode(),
				key,
				//JsonUtil.toJson(hb),
				hb,
				24*60*60L);
	}
	
	/**
	 * 取得心跳信息
	 * @param deviceCode
	 */
/*	public static HeartBeatBean getHeartbeat(String deviceCode){
		Object obj = SysCache.getInstance().hget(DEVICE_HEARTBEAT,deviceCode);
		if (obj == null){return null;}
		else{return JsonUtil.fromJson(obj.toString(), HeartBeatBean.class);}
	}
*/	
	/**
	 * 用户登录成功以后保存到内存
	 * @param loginName
	 * @param session
	 */
	public static void setLoginUser(String loginName ,Object object){
		SysCache.getInstance().hset(VAR_OPERATOR_LOGIN_MSG, loginName, object,1*30l);
	}
	
	public static void expireLoginUser(String loginName ){
		SysCache.getInstance().setExpire(VAR_OPERATOR_LOGIN_MSG, loginName, 1*30l);
	}
	
	
	/**
	 * 取得用户登录成功以后的session
	 * @param loginName
	 * @return
	 */
	public static Object getLoginUserSession(String loginName){
		Object obj = SysCache.getInstance().hget(VAR_OPERATOR_LOGIN_MSG,loginName);
		if (obj == null){return null;}
		else{
			return obj;
		}
	}
	
	/**
	 * 取得所有登录的用户
	 * @return
	 */
	public static Map<Object,Object> getAllLoginUserSession(){
		return SysCache.getInstance().hmget(VAR_OPERATOR_LOGIN_MSG);
	}
	
	public static void removeLoginUser(String loginName){
		SysCache.getInstance().hdel(VAR_OPERATOR_LOGIN_MSG, loginName);
	}
	
	
/*	Object obj = SysCache.getInstance().hget(Constant.DEVICE_ONLINE_TIME,deviceCode);
	if (obj != null){
		device.getAttributes().put("status","online");
		device.setIndex(1);
	}
	
	SysCache.getInstance().hget(Constant.DEVICE_ONLINE_TIME,deviceCode);
*/	
}
