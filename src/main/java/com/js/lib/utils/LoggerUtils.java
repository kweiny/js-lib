package com.js.lib.utils;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.js.lib.log.Constant;

import lombok.extern.slf4j.Slf4j;

/**
 * 说明：
 * @类名: LoggerUtils
 *
 * @author   kenny
 * @Date	 2020年1月2日下午4:03:46
 */
@Slf4j
public class LoggerUtils {

    /**
     * 生成日志通用的数据
     * @return
     * @throws Throwable
     */
    public static Map<String,Object> generalParam() {
    	
        //日志的参数
        Map<String,Object>logParam = new HashMap<String,Object>();
        
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        String requestIp;
		try {
			requestIp = RequestUtils.getRequestIp(request);
			logParam.put("clientIp", requestIp);
		} catch (UnknownHostException e) {
			log.debug(">>>>> generalParam: 取客户端 IP异常");
			
		}
        String url=request.getRequestURL().toString();
        String traceCode = MDC.get(Constant.LOG_TRACE);
        
        logParam.put("traceCode", traceCode);
        logParam.put("url", url);
        log.debug(url);
         return logParam;
    }
}
