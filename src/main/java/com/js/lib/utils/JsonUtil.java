package com.js.lib.utils;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSON;
/**
 * 说明：GSON转map 数据类型不靠谱，改成 fastJson
 * @类名: JsonUtil
 *
 * @author   kenny
 * @Date	 2021年3月22日下午4:46:24
 */
public class JsonUtil {
	
	
	public static String toJson(Object obj){
		return JSON.toJSONString(obj);
	}
	
	public static <T> T fromJson(String json,Class<T> clazz){
		return JSONObject.parseObject(json, clazz);
	}
}
