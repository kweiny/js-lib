package com.js.lib.utils;

import com.js.lib.cache.SysCache;

/**
 * 说明：任务同步锁
 * @类名: TaskLockUtil
 *
 * @author   kenny
 * @Date	 2020年8月25日下午1:39:48
 */
public class TaskSyncLockUtil {
	public static void lock(String key){
		SysCache.getInstance().set(key, 1, 24*60*60L);
	}
	public static void lock(String key,long sec){
		SysCache.getInstance().set(key, 1, sec);
	}
	
	public static void unLock(String key){
		SysCache.getInstance().remove(key);
	}
	
	public static boolean isLock(String key){
		return SysCache.getInstance().get(key)!=null;
	}
}
