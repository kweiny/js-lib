package com.js.lib.utils;

import com.js.lib.result.ApiResult;
import com.js.lib.result.Result;
/**
 * 说明：
 * @类名: ResultUtil
 * <p>
 *
 * </p>
 * @author   kenny
 * @Date	 2021年12月8日下午2:51:34
 */
@SuppressWarnings("unchecked")
public class ResultUtil {
	public static ApiResult<?> ok(){
		return new ApiResult(0,true,"操作成功",null);
	}
	
	public static ApiResult<?> ok(String msg ){
		return new ApiResult(0,true,msg,null);
	}
	public static ApiResult<?> ok(int code,String msg ){
		return new ApiResult(code,true,msg,null);
	}
	public static ApiResult<?> ok(int code,String msg,Object obj ){
		return new ApiResult(code,true,msg,obj);
	}
	
	public static ApiResult<?> ok(Object obj ){
		return new ApiResult(0,true,"操作成功",obj);
	}
	public static ApiResult<?> ok(String msg,Object obj ){
		return new ApiResult(0,true,msg,obj);
	}
	
	public static ApiResult<?> error(){
		return new ApiResult(-1,false,"操作失败",null);
	}
	public static ApiResult<?> error(String msg){
		return new ApiResult(-1,false,msg,null);
	}
	
	public static ApiResult<?> error(Object obj){
		return new ApiResult(-1,false,"操作失败",obj);
	}
	public static ApiResult<?> error(String msg,Object obj){
		return new ApiResult(-1,false,msg,obj);
	}
	public static ApiResult<?> error(int code,String msg){
		return new ApiResult(code,false,msg,null);
	}
	public static ApiResult<?> error(int code,String msg,Object obj){
		return new ApiResult(code,false,msg,obj);
	}	
}
