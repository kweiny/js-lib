package com.js.lib.utils;

import java.util.UUID;

/**
 * 说明：id生成器工具
 * @类名: IdUtils
 *
 * @author   kenny
 * @Date	 2019年12月27日下午6:11:43
 */
public class IdUtils {

    /**
     * 不含"-"
     * @return a Id by UUID
     */
    public static String getIdByUUID() {
        String uuIdStr = UUID.randomUUID().toString();

        return uuIdStr.replaceAll("-", "");
    }

    /**
     * 包含"-"
     * @return UUID
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
}
